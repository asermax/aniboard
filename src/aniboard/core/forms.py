from django import forms


class ImportShowsForm(forms.Form):
    import_file = forms.FileField()
