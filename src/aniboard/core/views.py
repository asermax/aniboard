from django.db import models as django_models
from django.core import mail, signing
from django.conf import settings
from django.utils import timezone
from django.template import loader
from django.contrib import auth
from django.contrib.auth import models as auth_models
from django.contrib.sites import shortcuts as site_shortcuts
from rest_framework import views, viewsets, mixins, response, decorators,\
    permissions as core_permissions, status

from . import models, serializers, permissions, filters, backend, pagination


class UserRestrictedMixin(object):
    user_field = 'user'
    permission_classes = (
        core_permissions.IsAuthenticated,
        permissions.IsOwner,
    )

    def perform_create(self, serializer):
        serializer.save(**{self.user_field: self.request.user})


class UserFilterMixin(object):
    user_field = 'user'

    def get_queryset(self):
        queryset = super(UserFilterMixin, self).get_queryset()

        return queryset.filter(**{self.user_field: self.request.user})


class SessionView(views.APIView):
    permission_classes = (core_permissions.AllowAny,)

    def get(self, request, format=None):
        return response.Response({
            'authenticated': request.user.is_authenticated(),
            'is_staff': request.user.is_staff,
            'id': request.user.id,
            'username': request.user.username,
        })

    def post(self, request, format=None):
        user = auth.authenticate(**request.data)

        if user and user.is_active:
            auth.login(request, user)

        return self.get(request, format)

    def delete(self, request, format=None):
        if request.user.is_authenticated():
            auth.logout(request)

        return self.get(request, format)


class SendEmailMixin(object):
    email_subject = 'New email'
    email_template_name = None
    email_headers = None
    email_from = settings.SERVER_EMAIL
    email_to = [a[1] for a in settings.MANAGERS]

    def get_email_subject(self, **kwargs):
        return self.email_subject.format(**kwargs)

    def get_email_template_name(self, **kwargs):
        return self.email_template_name

    def get_email_message_context(self, **kwargs):
        kwargs['site'] = site_shortcuts.get_current_site(self.request)
        return kwargs

    def get_email_message(self, **kwargs):
        return loader.render_to_string(
            self.get_email_template_name(**kwargs),
            self.get_email_message_context(**kwargs)
        )

    def get_email_headers(self, **kwargs):
        return self.email_headers

    def get_email_from(self, **kwargs):
        return self.email_from

    def get_email_to(self, **kwargs):
        return self.email_to

    def send_email(self, **kwargs):
        # get the email parameters
        subject = '{}{}'.format(
            settings.EMAIL_SUBJECT_PREFIX,
            self.get_email_subject(**kwargs)
        )
        message = self.get_email_message(**kwargs)
        from_email = self.get_email_from(**kwargs)
        to_email = self.get_email_to(**kwargs)
        headers = self.get_email_headers(**kwargs)

        # send the email
        email = mail.EmailMessage(
            subject=subject,
            body=message,
            from_email=from_email,
            to=to_email,
            headers=headers,
        )
        email.send()


class FeedbackView(SendEmailMixin, views.APIView):
    permission_classes = (core_permissions.IsAuthenticated,)
    serializer = serializers.FeedbackSerializer
    email_subject = '{type}: {short_description}'
    email_template_name = 'core/email/feedback.txt'

    def get_email_headers(self, **kwargs):
        return {'Reply-To': kwargs['user'].email}

    def post(self, request, format=None):
        serializer = self.serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        feedback = serializer.validated_data
        feedback['user'] = request.user

        self.send_email(**feedback)

        # return the same data
        return response.Response(serializer.data)


class ShowViewSet(viewsets.ModelViewSet):
    queryset = models.Show.objects.all()
    serializer_class = serializers.ShowSerializer
    filter_class = filters.ShowFilterSet


class ShowEpisodeViewSet(viewsets.ModelViewSet):
    queryset = models.ShowEpisode.objects.all()
    serializer_class = serializers.ShowEpisodeSerializer
    filter_class = filters.ShowEpisodeFilterSet


class SourceViewSet(viewsets.ModelViewSet):
    queryset = models.Source.objects.all()
    serializer_class = serializers.SourceSerializer


class AiringDatetimeViewSet(viewsets.ModelViewSet):
    queryset = models.AiringDatetime.objects.all().filter(
        airing_datetime__gte=timezone.now().replace(
            hour=0, minute=0, second=0, microsecond=0,
        ) - timezone.timedelta(days=1)
    )
    serializer_class = serializers.AiringDatetimesSerializer


class ShowSourceViewSet(viewsets.ModelViewSet):
    queryset = models.ShowSource.objects.all()
    serializer_class = serializers.ShowSourceSerializer
    filter_class = filters.ShowSourceFilterSet


class UserShowViewSet(UserRestrictedMixin,
                      viewsets.ModelViewSet):
    queryset = models.UserShow.objects.all()
    serializer_class = serializers.UserShowSerializer
    filter_class = filters.UserShowFilterSet
    permission_classes = (
        permissions.IsOwner,
    )


class UserShowActionViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = models.UserShowAction.objects.all().select_related(
        'user_show', 'comment'
    )
    serializer_class = serializers.UserShowActionSerializer
    filter_class = filters.UserShowActionFilterSet


class UserShowCommentViewSet(viewsets.ModelViewSet):
    user_field = 'user_show.user'
    permission_classes = (
        core_permissions.IsAuthenticated,
        permissions.IsOwner,
    )
    queryset = models.UserShowComment.objects.all()
    serializer_class = serializers.UserShowCommentSerializer
    filter_class = filters.UserShowCommentFilterSet
    pagination_class = pagination.AniboardPagination


class BackendViewSet(mixins.ListModelMixin,
                     mixins.RetrieveModelMixin,
                     viewsets.GenericViewSet):
    queryset = models.Backend.objects.all()
    serializer_class = serializers.BackendSerializer
    permission_classes = (core_permissions.IsAuthenticated,)
    filter_fields = ('backend_name',)

    @decorators.detail_route(methods=('post',))
    def connect(self, request, pk=None):
        instance = self.get_object()
        credentials = request.data['credentials']
        b = backend.create_backend(instance.backend_name, request.user)

        try:
            b.authorize(**credentials)
        except backend.APIException:
            pass

        serializer = self.get_serializer(instance)
        return response.Response(serializer.data)

    @decorators.detail_route(methods=('post',))
    def disconnect(self, request, pk=None):
        instance = self.get_object()

        try:
            request.user.api_data.filter(backend=instance).delete()
        except models.UserAPIData.DoesNotExist:
            pass

        serializer = self.get_serializer(instance)
        return response.Response(serializer.data)


class SettingsViewSet(UserFilterMixin,
                      UserRestrictedMixin,
                      mixins.RetrieveModelMixin,
                      mixins.UpdateModelMixin,
                      viewsets.GenericViewSet):
    queryset = models.Settings.objects.all()
    serializer_class = serializers.SettingsSerializer

    def get_object(self, *args, **kwargs):
        return self.get_queryset().get()

    def list(self, *args, **kwargs):
        return self.retrieve(self, *args, **kwargs)


class UserViewSet(mixins.RetrieveModelMixin, mixins.ListModelMixin,
                  viewsets.GenericViewSet):
    queryset = auth_models.User.objects.all().extra(
        select={
            'lower_username': 'lower(auth_user.username)',
        },
    ).order_by('lower_username')
    serializer_class = serializers.UserSerializer
    filter_class = filters.UserFilterSet
    permission_classes = (core_permissions.IsAuthenticated,)
    pagination_class = pagination.AniboardPagination

    def get_queryset(self):
        return super(UserViewSet, self).get_queryset().exclude(
            id=self.request.user.id,
        )

    @decorators.detail_route(methods=('post',))
    def stalk(self, request, pk=None):
        stalker = request.user
        instance = self.get_object()

        stalking, created = models.Stalking.objects.get_or_create(
            stalker=stalker,
            victim=instance,
        )

        if not created:
            stalking.delete()

        serializer = self.get_serializer(instance)
        return response.Response(serializer.data)


class UserTraceViewSet(UserRestrictedMixin,
                       mixins.CreateModelMixin,
                       viewsets.GenericViewSet):
    queryset = models.UserTrace.objects.all()
    serializer_class = serializers.UserTraceSerializer
    filter_class = filters.UserTraceFilterSet

    @decorators.list_route(permission_classes=[core_permissions.AllowAny])
    def max_count(self, request):
        users_with_trace_count = auth_models.User.objects.filter(
            traces__in=self.filter_queryset(self.get_queryset()),
        ).annotate(
            traces_count=django_models.Count('traces'),
        )

        max_trace_count = users_with_trace_count.aggregate(
            max=django_models.Max('traces_count')
        )['max']

        # get the max or the current user
        users = users_with_trace_count.filter(traces_count=max_trace_count)
        user = users.first() or request.user

        serializer = serializers.UserSerializer(
            user, context=self.get_serializer_context()
        )
        return response.Response(serializer.data)


class UserRegistrationViewSet(SendEmailMixin,
                              mixins.CreateModelMixin,
                              viewsets.GenericViewSet
                              ):
    queryset = auth_models.User.objects.all()
    serializer_class = serializers.UserRegistrationSerializer
    permission_classes = (core_permissions.AllowAny,)
    email_template_name = 'core/email/registration.txt'
    email_subject = 'Dear {username}, activate your account in Tsuiseki'

    def get_email_to(self, **kwargs):
        return (kwargs['user'].email,)

    def perform_create(self, serializer):
        user = serializer.save()

        self.send_email(**{
            'user': user,
            'username': user.username,
            'activation_code': signing.dumps(user.username),
        })


class UserActivationView(views.APIView):
    permission_classes = (core_permissions.AllowAny,)

    def post(self, request, format=None):
        serializer = serializers.UserActivationSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        try:
            username = signing.loads(
                serializer.validated_data['activation_code']
            )
            # activate the user
            user = auth_models.User.objects.get(username=username)
            user.is_active = True
            user.save()

            return response.Response(serializer.validated_data)
        except (auth_models.User.DoesNotExist, signing.BadSignature):
            return response.Response(
                {'activation_code': 'Invalid activation code'},
                status=status.HTTP_400_BAD_REQUEST,
            )


class PasswordForgotView(SendEmailMixin, views.APIView):
    permission_classes = (core_permissions.AllowAny,)
    email_template_name = 'core/email/forgot.txt'
    email_subject = 'Dear {username}, recover your password for your '\
        'Tsuiseki account'

    def get_email_to(self, **kwargs):
        return [kwargs['user'].email]

    def post(self, request, format=None):
        serializer = serializers.EmailSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        email = serializer.validated_data['email']
        user = auth_models.User.objects.filter(email=email).first()

        if user:
            # if we found an user, send the email with the recovery code
            self.send_email(**{
                'user': user,
                'username': user.username,
                'recovery_code': signing.dumps(user.username),
            })

        return response.Response(serializer.validated_data)


class PasswordRecoveryView(views.APIView):
    permission_classes = (core_permissions.AllowAny,)

    def post(self, request, format=None):
        serializer = serializers.PasswordRecoverySerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        try:
            username = signing.loads(
                serializer.validated_data['recovery_code']
            )

            # set new user password
            user = auth_models.User.objects.get(username=username)
            user.set_password(serializer.validated_data['password'])
            user.save()

            return response.Response(serializer.validated_data)
        except (auth_models.User.DoesNotExist, signing.BadSignature):
            return response.Response(
                {'activation_code': 'Invalid activation code'},
                status=status.HTTP_400_BAD_REQUEST,
            )


class PasswordChangeView(views.APIView):
    permission_classes = (core_permissions.IsAuthenticated,)

    def post(self, request, format=None):
        user = request.user
        serializer = serializers.PasswordChangeSerializer(
            data=request.data,
            context={'user': user}
        )
        serializer.is_valid(raise_exception=True)

        # set new user password
        user.set_password(serializer.validated_data['new_password'])
        user.save()

        return response.Response(serializer.validated_data)


class NoticeViewSet(viewsets.ModelViewSet):
    queryset = models.Notice.objects.all()
    serializer_class = serializers.NoticeSerializer
    filter_class = filters.NoticeFilterSet
    permission_classes = (core_permissions.IsAuthenticated,)
    pagination_class = pagination.AniboardPagination

    @decorators.detail_route(methods=('post',))
    def read(self, request, pk=None):
        instance = self.get_object()
        self.request.user.settings.last_notice_read = instance
        self.request.user.settings.save()

        serializer = self.get_serializer(instance)
        return response.Response(serializer.data)
