from __future__ import absolute_import

from django.utils import text, timezone
import requests
import json
from aniboard.core import models
from aniboard.core.importer import base


class JSONShowsParser(base.BaseParser):
    def parse_entries(self, import_text):
        return json.loads(import_text)['merge']

    def parse_fails(self, import_text):
        load = json.loads(import_text)['fails']
        text = ''
        if 'lc' in load and len(load['lc']) > 0:
            text += 'LiveChart\n'

            for fail in load['lc']:
                text += fail + '\n'
        if 'sb' in load and len(load['sb']) > 0:
            text += '\n\nSyoboi\n'

            for fail in load['sb']:
                text += fail + '\n'
        return text

    def preprocess_entry(self, entry):
        try:
            entry['slug'] = text.slugify(entry['title']['ro'][:100])
        except KeyError:
            raise base.PreprocessException(
                'Doesn\'t have a title, which is required'
            )

        # we have to download the image link
        if 'image' in entry and entry['image'] is not None:
            try:
                response = requests.get(entry['image'], stream=True)
                response.raw.decode_content
                response.raise_for_status()
                entry['img_content'] = response
            except Exception as ex:
                raise base.PreprocessException(
                    'The image couldn\'t be retrieved: {}'.format(ex)
                )

        if 'sources' in entry:
            for source_info in entry['sources']:
                # obtain the sources from the name
                try:
                    source = models.Source.objects.filter(
                        name_ja=source_info['name']
                    ).first()

                    if source is None:
                        source = models.Source.objects.create(
                            name=source_info['name'],
                            name_ja=source_info['name']
                        )

                    source_info['is_premiere'] = source_info.get(
                        'premiere', False
                    )
                    source_info['source'] = source
                    if 'eps' in source_info:
                        for ep in source_info['eps']:
                            if 'datetime' in ep:
                                ep['datetime'] = timezone.make_aware(
                                    timezone.datetime.utcfromtimestamp(
                                        ep['datetime']
                                    ),
                                    timezone.get_current_timezone()
                                )
                except ValueError:
                    raise base.PreprocessException(
                        'The date/time couldn\'t be parsed'
                    )

    def _update_image(self, entry, show):
        """ Updates the image for a show. """
        self.update_image(
            entry['image'],
            entry['img_content'].raw.read(),
            show
        )

    def _update_api_datas(self, entry, show):
        """ Creates and/or updates the related api data. """
        backends_keys = [
            ('mal', 'mal'), ('kitsu', 'kitsu'),
            ('anilist', 'anilist')
        ]
        backends = {}

        for api_key, backend in backends_keys:
            if api_key in entry['id']:
                backends[backend] = entry['id'][api_key]

        self.update_api_datas(backends, show)

    def _update_show_sources(self, entry, show):
        if 'sources' in entry:
            for source in entry['sources']:
                self.update_show_source(source, show)

    def create_show(self, entry):
        # initial show
        show = models.Show.objects.create(
            slug=entry['slug'],
            name=entry['title']['ro'],
            name_ja=entry['title'].get('ja', None),
            number_episodes=entry.get('eps', None),
            syoboi_id=entry['id'].get('sb', None)
        )

        if 'image' in entry and entry['image'] is not None:
            # add the image if there's one
            self._update_image(entry, show)

        # show source
        self._update_show_sources(entry, show)

        # api datas
        self._update_api_datas(entry, show)

        return show

    def update_show(self, entry, show):
        # update base fields
        if 'ja' in entry['title']:
            show.name_ja = entry['title']['ja']

        if 'eps' in entry:
            show.number_episodes = entry['eps']

        show.save()

        if 'image' in entry and entry['image'] is not None:
            # the image can be missing
            self._update_image(entry, show)

        # update api datas if needed
        self._update_api_datas(entry, show)

        # update show source
        self._update_show_sources(entry, show)

    def postprocess_entry(self, entry):
        if 'image' in entry and entry['image'] is not None and \
           entry['img_content'] is not None:
            # we have to make sure the request is closed
            entry['img_content'].close()
