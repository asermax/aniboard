const request = require('request').defaults({jar: true});
const cheerio = require('cheerio');
const keys = require('./keys.js');

const URL = (spec) => `https://www.livechart.me/${spec}`;

let fecker = 0;

module.exports = (spec, cb) => {
  let url = URL(spec? spec : '');

  request({
    url: url,
    headers: {
      'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:105.0) Gecko/20100101 Firefox/105.0',
      Cookie: 'preferences=%7B%22leftovers%22%3Afalse%7D; cf_clearance=Ru930finigQ4picav0sRYqvXtYLePKcAmT62nQhJWqo-1663645068-0-150;',
    }
  }, (err, res, body) => {
    console.log(body)
    let $ = cheerio.load(body);
    //console.log(body)

    let season = $('h1').text().match(/(.+) Anime/i)[1];
    console.log(season);

    getShows($('.anime'), (shows) => {
      cb(shows);
    })
  })
}

function getShows(card, cb) {
  let shows = [];

  let amt = card.length;
  let count = 0;
  card.each(function(i) {
    let $ = cheerio.load(this);
    let show = {
      title: {},
      id: {},
      eps: null,
      image: null,
      sources: []
    };

    let title = $('.anime');
    let lcID = title.attr('data-anime-id');
    show.title.ro = title.attr('data-romaji');
    show.title.ja = title.attr('data-native');
    show.title.en = title.attr('data-english');

    let eps = $('.anime-episodes').text().match(/(\d+) eps/);
    if(eps) show.eps = parseInt(eps[1]);

    let links = [
      '.mal-icon', '.anime-planet-icon', '.anidb-icon', '.kitsu-icon',
      '.crunchyroll-icon', '.funimation-icon', '.anilist-icon'
    ];
    for(let link of links) {
      if($(link).length) {
        let url = $(link).attr('href');
        if(/myanimelist/i.test(url)) {
          show.id.mal = url.match(/\/anime\/(\d+)/i)[1];
        }
        else if(/anime-planet/i.test(url))
          show.id.ap = url.match(/\/anime\/([^\/]+)/i)[1];
        else if(/anidb/i.test(url))
          show.id.adb = url.split('.net/')[1];
        else if(/kitsu/i.test(url))
          show.id.kitsu = url.match(/\/anime\/([^\/]+)/i)[1];
        else if(/anilist/i.test(url))
          show.id.anilist = url.match(/\/anime\/([^\/]+)/i)[1];
        else if(/crunchyroll/i.test(url) && /series/i.test(url))
          show.sources.push({
            'name': 'Crunchyroll',
            'id': url.match(/series\/([^\/]+)/i)[1],
            'eps': []
          });
        else if(/funimation/i.test(url))
          show.sources.push({
            'name': 'FUNimation',
            'id': url.match(/shows\/([^\/]+)/i)[1],
            'eps': []
          });
      }
    }

    //leftover from when we needed another async call, keeping for now
    next();

    function next() {
      if($('.preview-icon').length) {
        let getVidData = function() {
          request({url: `https://www.livechart.me/api/v1/anime/${lcID}/videos?country_code=AUTO`, headers: {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/65.0'}}, (err, res, body) => {
            let data = JSON.parse(body)['items'];//console.log(data, lcID)
            if(data.length == 0) {
              finish('No video for this region');
              return;
            }
            let pref = {
              site: [/youtu\.?be/i, /dailymotion/i],
              title: [/PV/i, /CM/i, /Teaser/i]
            }
            let options = data.filter((datum) => {
              for(let p of pref.site) {
                if(p.test(datum.url)) {
                  for(let t of pref.title) {
                    if(t.test(datum.title)) {
                      return true;
                      break;
                    }
                  }
                }
              }
              return false;
            })
            console.log(`${lcID} has ${options.length} options for thumbs`);
            if(options.length) {
              let vidurl = options[0].url;
              getImg(vidurl, (img) => {
                show.image = img;
                finish(vidurl);
              })
            } else
              finish('No good thumb');
          })
        };
        fecker++;
        setTimeout(getVidData, (fecker*2000));
        if(fecker > 10) fecker = 0;
      } else {
        finish('No videos');
      }
    }

    function finish(vidstat) {
      shows.push(show);
      count++;
      console.log(`${count}/${amt}: ${lcID}|${show.title.ro}\n${vidstat}`);
      if(amt == count)
        cb(shows);
    }
  })
}

function getAL(idMal, cb) {
  request.post({
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    },
    url: 'https://graphql.anilist.co',
    body: JSON.stringify({
      query: `query { Media(idMal:${idMal}, type: ANIME) { id } }`
    })
  }, function(err, res, body) {
    body = JSON.parse(body);
    if(body.data.Media)
      cb(body.data.Media.id);
    else
      cb(null);
  });
}

function getImg(pv, cb) {
  if(/youtu\.?be/i.test(pv)) {
    ytimgURL(pv.match(/(?:v=|\.be\/)([a-zA-Z0-9_-]{11})/)[1], function(img) {
      cb(img);
    });
  } else if(/dailymotion/i.test(pv)) {
    dmimgURL(pv.match(/video\/([a-zA-Z0-9]+)/)[1], function(img) {
      cb(img);
    });
  } else
    cb(null);
}

function dmimgURL(id, cb) {
  request({
    url:`https://api.dailymotion.com/video/${id}`,
    qs: {
      fields: 'thumbnail_url'
    }
  }, function(err, res, body) {
    let data = JSON.parse(body);
    cb(data.thumbnail_url);
  });
}
function ytimgURL(id, cb) {
  request({
    url: 'https://www.googleapis.com/youtube/v3/videos',
    qs: {
      part: 'snippet',
      id: id,
      fields: 'items/snippet/thumbnails',
      key: keys.google
    }
  }, function(err, res, body) {
    let data = JSON.parse(body).items[0];
    if(!data) {
      cb(null);
      return;
    }
    data = data.snippet.thumbnails;

    if(data.maxres)
      cb(data.maxres.url);
    else if(data.standard)
      cb(data.standard.url);
    else if(data.high)
      cb(data.high.url);
    else
      cb(null);
  });
}
