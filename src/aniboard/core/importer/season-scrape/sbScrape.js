const request = require('request');
const cheerio = require('cheerio');
const moment = require('moment');

const URL = (spec) => `http://cal.syoboi.jp/quarter/${spec}`;

module.exports = (spec, cb) => {
  let url = URL(spec? spec : '');
  
  request({url: url}, (err, res, body) => {
    let $ = cheerio.load(body);
    
    let season = $('#main h1').text();
    console.log(season);
    
    getShows($('.titlesDetail > li'), (shows) => {
      cb(shows);
    })
  })
}

function getShows(card, cb) {
  let shows = [];
  
  card.each(function(i) {
    let $ = cheerio.load(this);
    let show = {
      title: {},
      id: {},
      sources: []
    };
    
    show.title.ja = $('.title').text().trim();
    let id = $('.title').attr('href').match(/tid\/(\d+)/i)[1];
    show.id.sb = id;
    
    if($('.progs').length) {
      $('.progs li').each(function(ind) {
        let $ = cheerio.load(this);
        let source = {
          name: '',
          id: id,
          premiere: false,
          eps: []
        };
        
        if(ind === 0) source.premiere = true; 
        source.name = $('.ChName').text();
        
        let ep = {};
        ep.ep = 1;
        let datetime = $('.StTime').text();
        let date = datetime.split(' (')[0];
        let time = datetime.split(') ')[1];
        time = time.replace(' ','0');
        ep.datetime = moment(toUTC(date, time)).unix();
        
        source.eps.push(ep);
        show.sources.push(source);
      })
    }
    shows.push(show);
  })
  cb(shows);
}

function toUTC(date, time) {
  let time_c = parseInt(time.replace(':',''), 10);
  if(time_c >= 2400) {
    time = (time_c - 2400).toString();
    while(time.length < 4) {
      time = 0+time;
    }
    time = time.substr(0,2) + ':' + time.substr(2);
    date = moment(date).add(1,'d').format('YYYY-MM-DD');
  }
  let datetime = date+' '+time+':00+09:00';
  datetime = moment(datetime).utc();//.format('YYYY-MM-DD HH:mm');
  return datetime;
}
