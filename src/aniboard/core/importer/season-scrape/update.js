const sbUpdate = require('./sbUpdate.js');
const crUpdate = require('./crUpdate.js');
const funUpdate = require('./funUpdate.js');
const dsUpdate = require('./dsUpdate.js');

module.exports = (show_sources, cb) => {
  let groups = {
    'Crunchyroll': [],
    'FUNimation': [],
    //'DAISUKI': [],
    //'Syoboi': []
  };
  let updaters = {
    'Crunchyroll': crUpdate,
    'FUNimation': funUpdate,
    'DAISUKI': dsUpdate,
    'Syoboi': sbUpdate
  };
  let promises = [];
  let master = {
    'updated': [],
    'not_updated': []
  };
  
  for(let show_source of show_sources) {
    //console.log('found a show source');
    if(groups.hasOwnProperty(show_source['name'])) {
      //console.log(`grouped in ${show_source['name']}`);
      groups[show_source['name']].push(JSON.parse(JSON.stringify(show_source)));
    }
    else {
      //console.log('grouped in Syoboi');
      //groups['Syoboi'].push(JSON.parse(JSON.stringify(show_source)));
    }
  }
  for(let group in groups) {
    if(groups[group].length) {
      console.log(`group ${group} has members`);
      //console.log(JSON.stringify(groups[group]));
      promises.push(
        new Promise((resolve, reject) => {
          updaters[group](groups[group], (update) => {
            resolve(update);
          });
        })
      );
    }
  }
  
  Promise.all(promises).then((updates) => {
    console.log('all promises resolved.');
    for(let update of updates) {
      master['updated'] = master['updated'].concat(update['updated']);
      master['not_updated'] = master['not_updated'].concat(update['not_updated']);
    }
    cb(master);
  })
}
