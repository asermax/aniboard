const match = require('./match.js');

module.exports = (lc, sb) => {
  let result = {
    merge: [],
    fails: {
      lc: [],
      sb: []
    },
    matches: []
  }
  
  for(let i in lc) {
    let show_lc = lc[i];
    
    let found = false;
    let finds = [];
    for(let show_sb of sb) {
      let val = match(show_lc.title.ja, show_sb.title.ja);
      let _val = match(show_lc.title.ro, show_sb.title.ja);
      if(_val > val) val = _val;
      if(val > 0.45) {
        let found = {
          info: {
            lc: show_lc.title.ja,
            sb: show_sb.title.ja,
            value: val
          },
          lc: JSON.parse(JSON.stringify(show_lc)),
          sb: JSON.parse(JSON.stringify(show_sb))
        };
        //sb = sb.filter((val) => val != show_sb);
        finds.push(found);
      }
    }
    if(finds.length) {
      finds.sort((a, b) => {
        if (a['info']['value'] < b['info']['value']) return 1;
        else if (a['info']['value'] > b['info']['value']) return -1;
        else return 0;
      });
      found = finds[0];
    }
    
    if(found) {
      result.matches.push(found);
      //result.merge.push(merge(found['lc'], found['sb']));
    }
    else
      result.fails.lc.push(show_lc.title.ja);
  }
  
  let matchset = JSON.parse(JSON.stringify(result.matches));
  for(let match of matchset) {
    let dup = matchset.find((other) => {
      return JSON.stringify(match.sb) === JSON.stringify(other.sb) && JSON.stringify(match.lc) !== JSON.stringify(other.lc)
    });
    if(dup) {
      console.log(`Duplicate: ${JSON.stringify(match['info'])} and ${JSON.stringify(dup['info'])}`);
      let best = (match['info']['value'] > dup['info']['value'])? match : dup;
      let notbest = (match['info']['value'] > dup['info']['value'])? dup : match;
      result.merge.push(merge(best['lc'], best['sb']));
      result.fails.lc.push(notbest['info']['lc']);
      matchset = matchset.filter((val) => val !== dup && val !== match);
      result.matches = result.matches.filter((val) => JSON.stringify(val) !== JSON.stringify(notbest));
    }
    else
      result.merge.push(merge(match['lc'], match['sb']));
    sb = sb.filter((val) => JSON.stringify(val) != JSON.stringify(match['sb']));
  }
  for(let show_sb of sb)
    result.fails.sb.push(show_sb.title.ja);
  for(let i in result.matches)
    result.matches[i] = result.matches[i]['info'];
  
  return result;
}

function merge(show_lc, show_sb) {
  let result = JSON.parse(JSON.stringify(show_lc));
  
  result.title.ja = show_sb.title.ja;
  result.id.sb = show_sb.id.sb;
  result.sources = result.sources.concat(show_sb.sources);
  
  return result;
}
