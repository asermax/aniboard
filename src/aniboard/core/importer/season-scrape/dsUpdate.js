const request = require('request');
const keys = require('./keys.js');

module.exports = (show_sources, cb) => {
  console.log('Connecting to DAISUKI...');
  let update = {
    'updated': [],
    'not_updated': []
  };
  request({
    url: 'https://graph.facebook.com/v2.6/144642022361236/feed/',
    qs: {
      'access_token': keys.facebook,
      limit: 100
    }
  }, (err, res, body) => {
    console.log('Retrieved from DAISUKI!');
    let data = JSON.parse(body);
    
    cb(update);
  })
}
