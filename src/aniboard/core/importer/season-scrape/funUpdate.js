const request = require('request');
const cheerio = require('cheerio');
const moment = require('moment');

module.exports = (show_sources, cb) => {
  console.log(show_sources);
  console.log('Connecting to FUNimation...');
  let update = {
    'updated': [],
    'not_updated': []
  };
  request({
    url: 'http://www.funimation.com/videos/simulcasts_videos'
  }, (err, res, body) => {
    console.log('Retrieved FUNimation!');
    let $ = cheerio.load(body);
    
    let videos = [];
    $('.elite.simulcast').each(function() {
      let video = parseVideo($(this));
      if(video)
        videos.push(video);
    })
    console.log(videos);
    
    let releases = [];
    $('.item-cell.two p').each(function() {
      let release = parseItem($(this), videos);
      if(release)
        releases.push(release);
    })
    console.log(releases);
    
    thrushow:
    for(let show_source of show_sources) {
      if(!releases.length) {
        update['not_updated'].push(show_source);
        break;
      }
      for(let release of releases) {
        if(show_source['id'] == release['id']) {
          let diff = findDiff(show_source, release)
          if(diff)
            update['updated'].push(diff);
          else
            update['not_updated'].push(show_source);
          continue thrushow;
        }
      }
      update['not_updated'].push(show_source);
    }
    
    cb(update);
  })
}

function parseVideo(item) {
  let video = {
    'id': null,
    'ep': null
  }
  
  let elite = item.find('.bottom-links-r .elite');
  if(!elite.length || /Dub/i.test(elite.first().text()))
    return false;
  
  let ep = item.find('.watchLinks').first();
  video.id = ep.attr('href').match(/shows\/(.+)\/videos/i)[1];
  video.ep = parseInt(ep.text().match(/[Episode|Special]\s+(\d+)/i)[1], 10);
  
  return video;
}

function parseItem(item, videos) {
  let release = {
    id: '',
    eps: []
  };
  
  let time = item.find('span').first().text();
  if(/Dub/i.test(time))
    return false;
  time = moment(time.match(/([\d\:\spam]+)/i)[1], 'h:mm a');
  let day = item.closest('.item-cell.two').find('.day').first().text();
  day = getDayNum(day);
  let datetime = moment().day(day).hour(time.hour()).minute(time.minute());
  
  release['id'] = item.find('.item-title').first().attr('href').match(/shows\/([^\/]+)/i)[1];
  
  let ep = {
    'ep': null,
    'datetime': datetime.unix()
  }

  // for(let video of videos)
  //   if(release['id'] == video['id'])
  //     ep['ep'] = video['ep'] + 1;
  release['eps'].push(ep);
  
  return release;
}

function getDayNum(day) {
  switch(day) {
    case 'SUN':
      return 0;
    case 'MON':
      return 1;
    case 'TUE':
      return 2;
    case 'WED':
      return 3;
    case 'THU':
      return 4;
    case 'FRI':
      return 5;
    case 'SAT':
      return 6;
  }
}

function findDiff(original, nu) {
  let old = JSON.parse(JSON.stringify(original));
  
  thrunu:
  for(let nep of nu['eps']) {
    for(let oep in old['eps']) {
      if(old['eps'][oep]['ep'] == nep['ep']) {
        old['eps'][oep]['datetime'] = nep['datetime'];
        continue thrunu;
      }
    }
    old['eps'].push(nep);
  }
  
  if(JSON.stringify(old) === JSON.stringify(original))
    return false;
  else
    return old;
}
