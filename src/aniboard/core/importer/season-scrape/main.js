const fs = require('fs');
const jf = require('jsonfile');
const lcScrape = require('./lcScrape.js');
const sbScrape = require('./sbScrape.js');
const merge = require('./merge.js');
const match = require('./match.js');
const update = require('./update.js');

function quarter(season) {
  let year = new Date().getFullYear();
  switch(season) {
    case 'winter':
      return (year+1)+'q'+1;
      break;
    case 'spring':
      return year+'q'+2;
      break;
    case 'summer':
      return year+'q'+3;
      break;
    case 'fall':
    case 'autumn':
      return year+'q'+4;
      break;
  }
}

function season() {
  let result = {
    lc: null,
    sb: null
  };
  
  if(process.argv[3]) {
    let argu = process.argv[3];
    let year = (argu == 'winter')? new Date().getFullYear() + 1 : new Date().getFullYear();
    let season = (argu == 'autumn')? 'fall' : argu;

    result.lc = season+'-'+year;
    result.sb = quarter(argu);
  }
  
  return result;
}

function saveLC(cb) {
  lcScrape(season().lc, (shows) => {
    fs.writeFileSync('./livechart.json', JSON.stringify(shows, null, 2));
    if(cb) cb(shows);
  })
}
function saveSB(cb) {
  sbScrape(season().sb, (shows) => {
    fs.writeFileSync('./syoboi.json', JSON.stringify(shows, null, 2));
    if(cb) cb(shows);
  })
}

function updateShows(shows, cb) {
  let updated_shows = JSON.parse(JSON.stringify(shows));
  let show_sources = [];
  for(let show of shows)
    for(let show_source of show['sources'])
      show_sources.push(show_source);
  
  update(show_sources, (updated) => {
    fs.writeFileSync('./updates.json', JSON.stringify(updated, null, 2));
    
    for(let i in updated_shows) {
      let show = updated_shows[i];
      for(let j in show['sources']) {
        let show_source = show['sources'][j];
        for(let u in updated['updated']) {
          let update = updated['updated'][u];
          if(show_source['id'] == update['id']) {
            if(show_source['name'] == update['name']) {
              updated_shows[i]['sources'][j] = update;
              break;
            }
          }
        }
      }
    }
    
    if(cb) cb(updated_shows);
  });
}

switch(process.argv[2]) {
  case 'livechart':
    saveLC();
    break;
  case 'syoboi':
    saveSB();
    break;
  case 'merge':
    fs.writeFileSync('./merge.json', JSON.stringify(merge(jf.readFileSync('./livechart.json'), jf.readFileSync('./syoboi.json')), null, 2));
    break;
  case 'full':
    saveLC((lc) => {
      saveSB((sb) => {
        let merged = merge(lc, sb);
        updateShows(merged['merge'], (updated) => {
          merged['merge'] = updated;
          fs.writeFileSync('./merge.json', JSON.stringify(merged, null, 2));
        });
      })
    })
    break;
  case 'match':
    console.log(match(process.argv[3], process.argv[4]));
    break;
  case 'update':
    let merged = merge(jf.readFileSync('./livechart.json'), jf.readFileSync('./syoboi.json'));
    updateShows(merged['merge'],(updated) => {
      merged['merge'] = updated;
      fs.writeFileSync('./merge.json', JSON.stringify(merged, null, 2));
    });
    //update(JSON.parse(process.argv[3]), (updates) => fs.writeFileSync('./updates.json', JSON.stringify(updates, null, 2)));
    break;
}
