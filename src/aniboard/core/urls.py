# -*- coding: utf-8 -*-
from django.conf import settings
from django.conf.urls import patterns, include, url
from rest_framework import routers

from . import views


router = routers.SimpleRouter()
router.register(r'shows', views.ShowViewSet)
router.register(r'show_episodes', views.ShowEpisodeViewSet)
router.register(r'sources', views.SourceViewSet)
router.register(r'show_sources', views.ShowSourceViewSet)
router.register(r'user_shows', views.UserShowViewSet)
router.register(r'airing_datetimes', views.AiringDatetimeViewSet)
router.register(r'user_show_actions', views.UserShowActionViewSet)
router.register(r'user_show_comments', views.UserShowCommentViewSet)
router.register(r'settings', views.SettingsViewSet)
router.register(r'backends', views.BackendViewSet)
router.register(r'users', views.UserViewSet)
router.register(r'user_traces', views.UserTraceViewSet)
router.register(r'notices', views.NoticeViewSet)
router.register(r'user_registrations', views.UserRegistrationViewSet)

urlpatterns = patterns(
    '',
    url(r'^api_auth/', include(
        'rest_framework.urls', namespace='rest_framework')),
    url(r'^api/sessions?/$', views.SessionView.as_view()),
    url(r'^api/feedbacks?/$', views.FeedbackView.as_view()),
    url(r'^api/user_activations?/$', views.UserActivationView.as_view()),
    url(r'^api/password_forgots?/$', views.PasswordForgotView.as_view()),
    url(r'^api/password_recover(?:y|ies)/$',
        views.PasswordRecoveryView.as_view()),
    url(r'^api/password_changes/$',
        views.PasswordChangeView.as_view()),
    url(r'^api/', include(router.urls)),
)

# habilitamos flush_testserver solamente cuando debug esta activado
if settings.DEBUG:
    from . import test_views
    urlpatterns += [
        url(r'^testserver/setup/$', test_views.test_setup),
        url(r'^testserver/teardown/$', test_views.test_teardown)
    ]
