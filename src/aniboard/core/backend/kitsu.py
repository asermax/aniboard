from django.conf import settings
import requests
import datetime
import logging
from . import BaseAPI, APIException, APIUnauthorizedException

LOGGER = logging.getLogger(__name__)
API_URL = 'https://kitsu.io/api/{}/{}'
API_VERSION = 'edge'
OAUTH = 'oauth'


class EntryNotFoundException(APIException):
    pass


class API(BaseAPI):
    status_mapping = {
        BaseAPI.BASE_STATUS.WATCHING: 'current',
        BaseAPI.BASE_STATUS.COMPLETED: 'completed',
        BaseAPI.BASE_STATUS.ONHOLD: 'on-hold',
        BaseAPI.BASE_STATUS.DROPPED: 'dropped',
        BaseAPI.BASE_STATUS.PLANTOWATCH: 'planned',
    }

    def __init__(self, **api_data):
        self._assign_token(api_data)

    def _preprocess_auth_data(self, data):
        data = dict(data)
        data['expires'] = (
            datetime.datetime.utcnow() +
            datetime.timedelta(seconds=data['expires_in'])
        ).timestamp()

        return data

    def _assign_token(self, data):
        if 'access_token' in data:
            self._access_token = data['access_token']

            # FIXME: eventually all tokens will be updated so we can remove
            # this workaround
            if 'expires' in data:
                self._expires = datetime.datetime.utcfromtimestamp(
                    data['expires']
                )
            else:
                self._expires = datetime.datetime.utcnow()

            self._refresh_token = data['refresh_token']
            self._user_id = data['user_id']
        else:
            self._access_token = None
            self._expires = None
            self._refresh_access_token = None
            self._user_id = None

    @property
    def authentication_expired(self):
        return datetime.datetime.utcnow() >= self._expires

    def refresh_authentication(self):
        params = {
            'grant_type': 'refresh_token',
            'client_id': settings.KITSU_CLIENT_ID,
            'client_secret': settings.KITSU_CLIENT_SECRET,
            'refresh_token': self._refresh_token,
        }
        response = self._api_call(
            'token',
            params,
            'post'
        )
        data = self._preprocess_auth_data(response)
        data['user_id'] = self._user_id
        self._assign_token(data)

        return data

    def _api_call(self, entrypoint, data=None, method='get'):
        '''Makes a call against the api.
        :param entrypoint: API method to call.
        :param data: data to include in the request.
        '''
        request_params = {}

        if method == 'get':
            request_params['params'] = data
        else:
            request_params['json'] = data

        auth_request = 'token' in entrypoint

        if auth_request:
            url = API_URL.format(OAUTH, entrypoint)
        else:
            url = API_URL.format(API_VERSION, entrypoint)

            # every request except for auth requests needs these headers
            request_params['headers'] = {
                'Authorization': 'Bearer {}'.format(self._access_token),
                'Accept': 'application/vnd.api+json',
                'Content-Type': 'application/vnd.api+json'
            }

        # call the api
        request_call = getattr(requests, method)

        try:
            LOGGER.debug('Calling api: %s, %s', url, request_params)

            response = request_call(
                url,
                **request_params
            )

            LOGGER.debug('Response: %s', response.text)

            response.raise_for_status()
        except requests.RequestException as e:
            text = '{} - {}'.format(str(e), e.response.text)

            if e.response.status_code == requests.codes.UNAUTHORIZED or \
               e.response.status_code == requests.codes.FORBIDDEN or \
               e.response.status_code == requests.codes.BAD_REQUEST:
                raise APIUnauthorizedException(text)
            else:
                raise APIException(text)

        try:
            json_response = response.json()

            if 'error' in json_response:
                # raise error
                raise APIException(json_response['error_message'])
        except (ValueError, TypeError):
            json_response = None

        return json_response

    def authenticate(self, **credentials):
        if not self._access_token:
            credentials.update({
                'grant_type': 'password',
                'client_id': settings.KITSU_CLIENT_ID,
                'client_secret': settings.KITSU_CLIENT_SECRET
            })
            api_data = self._api_call(
                'token',
                credentials,
                'post'
            )

            # search for the user id since we use it later
            user_response = self._api_call(
                'users',
                {'filter[name]': credentials['username']}
            )
            api_data['user_id'] = user_response['data'][0]['id']

            api_data = self._preprocess_auth_data(api_data)
            self._assign_token(api_data)

        return api_data

    def find_anime(self, slug):
        filter_params = {
            'filter[slug]': slug
        }

        response = self._api_call(
            'anime',
            filter_params
        )

        if len(response['data']) != 1:
            raise APIException(
                'Anime associated to {} was not found'.format(slug)
            )

        return response['data'][0]

    def _find_anime_id(self, slug):
        return self.find_anime(slug)['id']

    def _find_entry_id(self, slug):
        filter_params = {
            'filter[userId]': self._user_id,
            'filter[mediaId]': self._find_anime_id(slug)
        }
        response = self._api_call(
            'library-entries',
            filter_params
        )

        if len(response['data']) != 1:
            raise EntryNotFoundException(
                'Entry associated to {} was not found'.format(slug)
            )

        return response['data'][0]['id']

    def update_show(self, episodes_watched=None, rating=None, status=None,
                    id=None):
        try:
            entry_id = self._find_entry_id(id)
        except EntryNotFoundException:
            self.add_show(episodes_watched, status, id)
        else:
            attributes = {
                'status': self.get_status(status)
            }

            if episodes_watched is not None:
                attributes['progress'] = episodes_watched

            if rating is not None:
                # rating is based on 5 on hummingbird
                attributes['rating'] = rating / 2.

            data = {
                'data': {
                    'id': entry_id,
                    'type': 'libraryEntries',
                    'attributes': attributes
                }
            }

            self._api_call(
                'library-entries/{}'.format(entry_id),
                data,
                'patch'
            )

    def add_show(self, episodes_watched=None, status=None, id=None):
        attributes = {
            'status': self.get_status(status)
        }

        if episodes_watched is not None:
            attributes['progress'] = episodes_watched

        relationships = {
            'user': {
                'data': {
                    'type': 'users',
                    'id': self._user_id
                }
            },
            'media': {
                'data': {
                    'type': 'anime',
                    'id': self._find_anime_id(id)
                }
            }
        }

        data = {
            'data': {
                'type': 'libraryEntries',
                'attributes': attributes,
                'relationships': relationships
            }
        }

        self._api_call(
            'library-entries',
            data,
            'post'
        )

    def remove_show(self, id=None):
        entry_id = self._find_entry_id(id)

        self._api_call(
            'library-entries/{}'.format(entry_id),
            method='delete'
        )
