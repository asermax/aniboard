import requests
from . import BaseAPI, APIException, APIUnauthorizedException

API_URL = 'https://hummingbird.me/api/v1/{}'


class API(BaseAPI):
    status_mapping = {
        BaseAPI.BASE_STATUS.WATCHING: 'currently-watching',
        BaseAPI.BASE_STATUS.COMPLETED: 'completed',
        BaseAPI.BASE_STATUS.ONHOLD: 'on-hold',
        BaseAPI.BASE_STATUS.DROPPED: 'dropped',
        BaseAPI.BASE_STATUS.PLANTOWATCH: 'plan-to-watch',
    }

    def __init__(self, token=None):
        self._token = token

    def _api_call(self, entrypoint, params=None, method='get'):
        '''Makes a call against the api.
        :param entrypoint: API method to call.
        :param params: parameters to include in the request data.
        '''
        params = params or {}
        url = API_URL.format(entrypoint)

        # default params
        if self._token:
            params['auth_token'] = self._token

        # call the api
        request_call = getattr(requests, method)

        try:
            response = request_call(url, params=params)
            response.raise_for_status()
        except requests.HTTPError as e:
            text = '{} - {}'.format(str(e), e.response.text)

            if e.response.status_code == requests.codes.UNAUTHORIZED:
                raise APIUnauthorizedException(text)
            else:
                raise APIException(text)

        json_response = response.json()

        # raise error
        if isinstance(json_response, dict) and 'error' in json_response:
            raise APIException(json_response['error'])

        return json_response

    def authenticate(self, **credentials):
        if not self._token:
            self._token = self._api_call(
                'users/authenticate', credentials, 'post')
        return {'token': self._token}

    def find_show(self, id):
        return self._api_call('anime/{}'.format(id))

    def update_show(self, episodes_watched=None, rating=None, status=None,
                    id=None):
        data = {'status': self.get_status(status)}

        if episodes_watched is not None:
            data['episodes_watched'] = episodes_watched

        if rating is not None:
            # rating is based on 5 on hummingbird
            data['sane_rating_update'] = rating / 2.

        self._api_call(
            'libraries/{}'.format(id),
            data,
            'post'
        )

    def add_show(self, episodes_watched=None, status=None, id=None):
        self.update_show(
            episodes_watched=episodes_watched,
            status=status,
            id=id
        )

    def remove_show(self, id=None):
        self._api_call('libraries/{}/remove'.format(id), method='post')
