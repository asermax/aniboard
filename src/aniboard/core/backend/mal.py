from django.conf import settings
import requests
import datetime
import logging
from . import BaseAPI, APIException, APIUnauthorizedException

LOGGER = logging.getLogger(__name__)
API_URL = 'https://api.myanimelist.net/v2/{}'
AUTH_URL = 'https://myanimelist.net/v1/oauth2/token'
OAUTH = 'oauth2'


class API(BaseAPI):
    status_mapping = {
        BaseAPI.BASE_STATUS.WATCHING: 'watching',
        BaseAPI.BASE_STATUS.COMPLETED: 'completed',
        BaseAPI.BASE_STATUS.ONHOLD: 'on_hold',
        BaseAPI.BASE_STATUS.DROPPED: 'dropped',
        BaseAPI.BASE_STATUS.PLANTOWATCH: 'plan_to_watch',
    }

    def __init__(self, **api_data):
        self._assign_token(api_data)

    def _preprocess_auth_data(self, data):
        data = dict(data)
        data['expires'] = (
            datetime.datetime.utcnow() +
            datetime.timedelta(seconds=data['expires_in'])
        ).timestamp()

        return data

    def _assign_token(self, data):
        if 'access_token' in data:
            self._access_token = data['access_token']

            if 'expires' in data:
                self._expires = datetime.datetime.utcfromtimestamp(
                    data['expires_in']
                )
            else:
                self._expires = datetime.datetime.utcnow()

            self._refresh_token = data['refresh_token']
        else:
            self._access_token = None
            self._expires = None
            self._refresh_access_token = None

    @property
    def authentication_expired(self):
        return datetime.datetime.utcnow() >= self._expires

    def refresh_authentication(self):
        params = {
            'grant_type': 'refresh_token',
            'client_id': settings.MAL_CLIENT_ID,
            'client_secret': settings.MAL_CLIENT_SECRET,
            'refresh_token': self._refresh_token,
        }
        response = self._api_call(
            'token',
            params,
            'post'
        )
        data = self._preprocess_auth_data(response)
        self._assign_token(data)

        return data

    def _api_call(self, entrypoint, data=None, method='get'):
        '''Makes a call against the api.
        :param entrypoint: API method to call.
        :param data: data to include in the request.
        '''
        request_params = {}

        if method == 'get':
            request_params['params'] = data
        else:
            request_params['data'] = data

        auth_request = 'token' in entrypoint

        if auth_request:
            url = AUTH_URL
        else:
            url = API_URL.format(entrypoint)

            # every request except for auth requests needs these headers
            request_params['headers'] = {
                'Authorization': 'Bearer {}'.format(self._access_token),
            }

        # call the api
        request_call = getattr(requests, method)

        try:
            LOGGER.debug('Calling api: %s, %s', url, request_params)

            response = request_call(
                url,
                **request_params
            )

            LOGGER.debug('Response: %s', response.text)

            response.raise_for_status()
        except requests.RequestException as e:
            text = '{} - {}'.format(str(e), e.response.text)

            if e.response.status_code == requests.codes.UNAUTHORIZED or \
               e.response.status_code == requests.codes.FORBIDDEN or \
               e.response.status_code == requests.codes.BAD_REQUEST:
                raise APIUnauthorizedException(text)
            else:
                raise APIException(text)

        try:
            json_response = response.json()

            if 'error' in json_response:
                # raise error
                raise APIException(json_response['message'])
        except (ValueError, TypeError):
            json_response = None

        return json_response

    def authenticate(self, **credentials):
        if not self._access_token:
            credentials.update({
                'grant_type': 'authorization_code',
                'client_id': settings.MAL_CLIENT_ID,
                'client_secret': settings.MAL_CLIENT_SECRET,
                'code_verifier': settings.MAL_CODE_VERIFIER,
            })
            api_data = self._api_call(
                'token',
                credentials,
                'post'
            )

            api_data = self._preprocess_auth_data(api_data)
            self._assign_token(api_data)

        return api_data

    def update_show(self, episodes_watched=None, rating=None, status=None, id=None):
        data = {
            'status': self.get_status(status)
        }

        if episodes_watched is not None:
            data['num_watched_episodes'] = episodes_watched

        if rating is not None:
            data['score'] = rating

        self._api_call(
            'anime/{}/my_list_status'.format(id),
            data,
            'patch'
        )

    def add_show(self, episodes_watched=None, status=None, id=None):
        self.update_show(
            episodes_watched=episodes_watched,
            status=status,
            id=id,
        )

    def remove_show(self, id=None):
        self._api_call(
            'anime/{}/my_list_status'.format(id),
            method='delete'
        )
