import logging
import requests
from django.conf import settings
from . import BaseAPI, APIException

LOGGER = logging.getLogger(__name__)
AUTH_URL = 'https://anilist.co/api/v2/oauth/token'
GRAPHQL_URL = 'https://graphql.anilist.co'


class API(BaseAPI):
    status_mapping = {
        BaseAPI.BASE_STATUS.WATCHING: 'CURRENT',
        BaseAPI.BASE_STATUS.COMPLETED: 'COMPLETED',
        BaseAPI.BASE_STATUS.ONHOLD: 'PAUSED',
        BaseAPI.BASE_STATUS.DROPPED: 'DROPPED',
        BaseAPI.BASE_STATUS.PLANTOWATCH: 'PLANNING',
    }

    def __init__(self, **api_data):
        self._assign_token(api_data)

    def _assign_token(self, data):
        if 'access_token' in data:
            self._access_token = data['access_token']
        else:
            self._access_token = None

    def _api_call(self, call_type='graphql', data=None, method='post'):
        '''Makes a call against the api.
        :param entrypoint: API method to call.
        :param data: data to include in the request.
        '''
        url = None
        request_params = {
            'headers': {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            }
        }

        if call_type == 'auth':
            # auth request use url params for some reason
            url = AUTH_URL
            request_params['json'] = data
        else:
            # every request except for auth requests needs these headers
            # and uses encoded data instead of url params
            url = GRAPHQL_URL
            request_params['headers']['Authorization'] = \
                'Bearer {}'.format(self._access_token)
            request_params['json'] = data

        # call the api
        request_call = getattr(requests, method)

        try:
            LOGGER.debug('Calling api: %s, %s', url, request_params)

            response = request_call(
                url,
                **request_params
            )

            LOGGER.debug('Response: %s', response.text)

            response.raise_for_status()
        except requests.RequestException as e:
            text = '{} - {}'.format(str(e), e.response.text)
            raise APIException(text)

        try:
            json_response = response.json()

            if 'errors' in json_response:
                # raise error
                raise APIException(json_response['errors'])
        except (ValueError, TypeError):
            json_response = None

        return json_response['data'] \
            if call_type == 'graphql' \
            else json_response

    def authenticate(self, **credentials):
        if not self._access_token:
            credentials.update({
                'grant_type': 'authorization_code',
                'client_id': settings.ANILIST_CLIENT_ID,
                'client_secret': settings.ANILIST_CLIENT_SECRET,
                'redirect_uri': settings.ANILIST_REDIRECT_URI,
            })
            response = self._api_call('auth', credentials)
            self._assign_token(response)

        return response

    def _fetch_user(self):
        data = {}
        data['query'] = '''
            query {
                Viewer {
                    id
                }
            }
        '''

        response = self._api_call(data=data)

        return response['Viewer']

    def _fetch_user_show(self, id):
        # we need the current user to be able to fetch the show
        user = self._fetch_user()

        data = {}
        data['query'] = '''
            query ($id: Int, $userId: Int) {
                MediaList (mediaId: $id, userId: $userId) {
                    id
                }
            }
        '''
        data['variables'] = {
            'id': id,
            'userId': user['id']
        }

        response = self._api_call(data=data)

        return response['MediaList']

    def _save_show(self, media_id, status, episodes=None, rating=None):
        data = {}
        data['query'] = '''
            mutation ($mediaId: Int, $status: MediaListStatus, $progress: Int, $score: Int) {
                SaveMediaListEntry(mediaId: $mediaId, progress: $progress, scoreRaw: $score, status: $status) {
                    id
                }
            }
        '''

        data['variables'] = {
            'mediaId': media_id,
            'status': self.get_status(status),
        }

        if episodes is not None:
            data['variables']['progress'] = episodes

        if rating is not None:
            data['variables']['score'] = rating * 10

        return self._api_call(data=data)

    def update_show(self, episodes_watched=None, rating=None, status=None,
                    id=None):
        self._save_show(id, status, episodes_watched, rating)

    def add_show(self, episodes_watched=None, status=None, id=None):
        self._save_show(id, status, episodes_watched)

    def remove_show(self, id=None):
        show = self._fetch_user_show(id)

        data = {}
        data['query'] = '''
            mutation ($id: Int) {
                DeleteMediaListEntry(id: $id) {
                    deleted
                }
            }
        '''
        data['variables'] = {
            'id': show['id'],
        }

        self._api_call(data=data)
