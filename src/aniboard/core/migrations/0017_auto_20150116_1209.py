# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0016_auto_20150115_2243'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserShowComment',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('episode', models.PositiveIntegerField()),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('liked', models.BooleanField(default=True)),
                ('comment', models.CharField(null=True, max_length=140, blank=True)),
                ('user_show', models.ForeignKey(to='core.UserShow', related_name='comments')),
            ],
            options={
                'ordering': ('-modified',),
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='usershowcomment',
            unique_together=set([('user_show', 'episode')]),
        ),
        migrations.RemoveField(
            model_name='usershowaction',
            name='comment',
        ),
        migrations.RemoveField(
            model_name='usershowaction',
            name='liked',
        ),
    ]
