# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.utils import text as text_utils


def slugify(apps, schema_editor):
    Show = apps.get_model('core', 'Show')

    for show in Show.objects.all():
        show.slug = text_utils.slugify(show.name)[:100]
        show.save()


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0010_show_slug'),
    ]

    operations = [
        migrations.RunPython(slugify),
        migrations.AlterField(
            model_name='show',
            name='slug',
            field=models.SlugField(max_length=100, unique=True),
            preserve_default=True,
        ),
    ]
