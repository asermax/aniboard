# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0009_backend_auth_url'),
    ]

    operations = [
        migrations.AddField(
            model_name='show',
            name='slug',
            field=models.SlugField(null=True, max_length=100),
            preserve_default=True,
        ),
    ]
