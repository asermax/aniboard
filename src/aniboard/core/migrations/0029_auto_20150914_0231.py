# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0028_show_syoboi_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='showsource',
            name='is_premiere',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='source',
            name='is_simulcast',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
