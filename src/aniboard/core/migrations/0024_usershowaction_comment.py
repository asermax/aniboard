# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import aniboard.fields


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0023_settings_theme'),
    ]

    operations = [
        migrations.AddField(
            model_name='usershowaction',
            name='comment',
            field=aniboard.fields.LenientForeignObject(related_name='action', to='core.UserShowComment', from_fields=('user_show_id', 'episode'), to_fields=('user_show_id', 'episode'), null=True, blank=True),
            preserve_default=True,
        ),
    ]
