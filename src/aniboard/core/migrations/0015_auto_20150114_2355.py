# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def populate_default_language(apps, schema_editor):
    Show = apps.get_model('core', 'Source')
    Show.objects.all().update(name_en=models.F('name'))


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0014_auto_20150111_1416'),
    ]

    operations = [
        migrations.AddField(
            model_name='source',
            name='name_en',
            field=models.CharField(null=True, max_length=255),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='source',
            name='name_ja',
            field=models.CharField(null=True, max_length=255),
            preserve_default=True,
        ),
        migrations.RunPython(populate_default_language)
    ]
