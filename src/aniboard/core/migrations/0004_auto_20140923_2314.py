# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_showsource_starts_on'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='airingdateexception',
            name='show_source',
        ),
        migrations.DeleteModel(
            name='AiringDateException',
        ),
    ]
