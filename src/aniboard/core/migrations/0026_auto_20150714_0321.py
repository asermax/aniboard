# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0025_auto_20150704_1739'),
    ]

    operations = [
        migrations.AddField(
            model_name='showapidata',
            name='api_id',
            field=models.CharField(max_length=250, null=True),
        ),
        migrations.AddField(
            model_name='showapidata',
            name='episode_offset',
            field=models.PositiveIntegerField(default=0),
        ),
    ]
