# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0018_auto_20150116_1227'),
    ]

    operations = [
        migrations.CreateModel(
            name='ShowEpisode',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('like_count', models.PositiveIntegerField(default=0, editable=False)),
                ('dislike_count', models.PositiveIntegerField(default=0, editable=False)),
                ('episode', models.PositiveIntegerField()),
                ('show', models.ForeignKey(to='core.Show', related_name='episodes')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
    ]
