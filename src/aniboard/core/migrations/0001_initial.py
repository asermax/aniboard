# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import aniboard.core.models
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='AiringDateException',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('date', models.DateField()),
            ],
            options={
                'ordering': ('date',),
                'get_latest_by': 'date',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='AiringSchedule',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('order', models.PositiveSmallIntegerField(default=0)),
                ('monday', models.TimeField(null=True, blank=True)),
                ('tuesday', models.TimeField(null=True, blank=True)),
                ('wednesday', models.TimeField(null=True, blank=True)),
                ('thursday', models.TimeField(null=True, blank=True)),
                ('friday', models.TimeField(null=True, blank=True)),
                ('saturday', models.TimeField(null=True, blank=True)),
                ('sunday', models.TimeField(null=True, blank=True)),
            ],
            options={
                'ordering': ('order',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Backend',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('backend_name', models.CharField(unique=True, max_length=255)),
                ('name', models.CharField(max_length=255)),
                ('description', models.TextField()),
                ('logo', models.ImageField(upload_to=aniboard.core.models.backend_logo_path, max_length=200)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Notice',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('active', models.BooleanField(default=True)),
                ('date_time', models.DateTimeField(auto_now_add=True)),
                ('text', models.TextField()),
            ],
            options={
                'ordering': ('-date_time',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RegistrationCode',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('code', models.CharField(max_length=20)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('used', models.DateTimeField(null=True, blank=True)),
                ('user', models.ForeignKey(null=True, to=settings.AUTH_USER_MODEL, blank=True)),
            ],
            options={
                'ordering': ('used', 'user'),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Settings',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('ask_rating', models.BooleanField(default=True)),
                ('last_notice_read', models.ForeignKey(null=True, to='core.Notice', blank=True)),
                ('user', models.OneToOneField(related_name='settings', to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Show',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('name', models.CharField(max_length=500)),
                ('airing', models.BooleanField(default=True)),
                ('number_episodes', models.PositiveIntegerField(null=True, verbose_name='number of episodes', blank=True)),
                ('cover_image', models.ImageField(upload_to=aniboard.core.models.cover_image_path, max_length=200)),
            ],
            options={
                'ordering': ('name',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ShowAPIData',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('data', jsonfield.fields.JSONField(default={})),
                ('backend', models.ForeignKey(to='core.Backend')),
                ('show', models.ForeignKey(to='core.Show', related_name='api_data')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ShowSource',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('last_aired_episode', models.PositiveIntegerField(default=0)),
                ('previous_airing_datetime', models.DateTimeField(null=True, blank=True)),
                ('next_airing_datetime', models.DateTimeField(null=True, blank=True)),
                ('show', models.ForeignKey(to='core.Show', related_name='show_sources')),
            ],
            options={
                'ordering': ('show__name', 'source__name'),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Source',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('name', models.CharField(max_length=255)),
            ],
            options={
                'ordering': ('name',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Stalking',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('stalker', models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='stalks')),
                ('victim', models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='stalked_by')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UserAPIData',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('data', jsonfield.fields.JSONField(default={})),
                ('backend', models.ForeignKey(to='core.Backend')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='api_data')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UserShow',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('last_episode_watched', models.PositiveIntegerField(default=0)),
                ('dropped', models.BooleanField(default=False)),
                ('rating', models.PositiveSmallIntegerField(null=True, blank=True)),
                ('show_source', models.ForeignKey(to='core.ShowSource', related_name='user_shows')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='shows')),
            ],
            options={
                'ordering': ('show_source__next_airing_datetime',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UserShowAction',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('episode', models.PositiveIntegerField()),
                ('action_datetime', models.DateTimeField(auto_now_add=True)),
                ('user_show', models.ForeignKey(to='core.UserShow', related_name='actions')),
            ],
            options={
                'ordering': ('-action_datetime',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UserTrace',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('date_time', models.DateTimeField(auto_now_add=True)),
                ('trace', models.CharField(blank=True, max_length=100)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='traces')),
            ],
            options={
                'ordering': ('-date_time',),
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='usershow',
            unique_together=set([('user', 'show_source')]),
        ),
        migrations.AlterUniqueTogether(
            name='userapidata',
            unique_together=set([('user', 'backend')]),
        ),
        migrations.AlterUniqueTogether(
            name='stalking',
            unique_together=set([('stalker', 'victim')]),
        ),
        migrations.AddField(
            model_name='showsource',
            name='source',
            field=models.ForeignKey(to='core.Source'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='showapidata',
            unique_together=set([('show', 'backend')]),
        ),
        migrations.AddField(
            model_name='show',
            name='sources',
            field=models.ManyToManyField(to='core.Source', related_name='shows', through='core.ShowSource'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='airingschedule',
            name='show_source',
            field=models.ForeignKey(to='core.ShowSource', related_name='schedules'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='airingdateexception',
            name='show_source',
            field=models.ForeignKey(to='core.ShowSource', related_name='airing_date_exceptions'),
            preserve_default=True,
        ),
    ]
