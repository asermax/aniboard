# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0013_airingdatetime_skip'),
    ]

    operations = [
        migrations.AddField(
            model_name='usershowaction',
            name='comment',
            field=models.CharField(blank=True, null=True, max_length=140),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='usershowaction',
            name='liked',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
