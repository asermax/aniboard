{% extends 'core/email/base.txt' %}
{% block content %}
Dear {{ username }}:
Thank you for creating an account in Tsuiseki!
To activate your account, please go to http://{{ site.domain }}/account/activate/{{ activation_code }}

After loging in, although not necessary, you may want to connect Tsuiseki to your accounts on other anime tracking sites like MAL, Hummingbird, or AniList (doing so will automatically update all connected sites as you update Tsuiseki). For that, you can click on the logo at the top right once you're logged in and choose Settings from the menu. From there you can click on the link(s) next to the site(s) you want to connect your account with and follow the instructions. 
It's important to perform this step before adding any series to your list in the case you want to connect to your MAL account, given a limitation on their API which causes shows added before connecting the account to fail to update correctly. 

We hope you enjoy the site! Feel free to contact us (by the feedback button on tsuiseki) with any comments or concerns you may have (something is broken, info is wrong, suggestion for better cover image, feature suggestions, etc). Feedback is always appreciated, especially this early in the game.
{% endblock %}
