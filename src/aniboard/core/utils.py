from django.conf import settings
from django.core import mail
from django.template import loader
from django.contrib.sites import shortcuts as site_shortcuts


def send_mail(subject, template, to_email, from_email=None, context=None,
              attachments=(), **kwargs):
    if not from_email:
        from_email = settings.SERVER_EMAIL
    if not context:
        context = {}

    context['site'] = site_shortcuts.get_current_site(None)
    message = loader.render_to_string(template, dictionary=context, **kwargs)

    email = mail.EmailMessage(subject, message, from_email, to_email)

    for file_path, filename, mimetype in attachments:
        with open(file_path, 'r') as attachment:
            email.attach(filename, attachment.read(), mimetype)

    email.send()
