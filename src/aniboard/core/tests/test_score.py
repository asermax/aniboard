from django import test
from django.utils import timezone
from django.core.files import uploadedfile
from .. import models


class ScoreTestCase(test.TestCase):
    def setUp(self):
        # create a couple of test shows an it's sources
        mock_image = uploadedfile.SimpleUploadedFile(
            'mock',
            b'content',
            'image/png'
        )

        # sources
        source1 = models.Source.objects.create(name='Source1')

        # shows
        show1 = models.Show.objects.create(
            name='Show1',
            slug='show1',
            cover_image=mock_image
        )

        # showsources
        show_source = models.ShowSource.objects.create(
            show=show1,
            source=source1,
            starts_on=timezone.now()
        )

        # create an user and assign the show
        user = models.auth_models.User.objects.create_user(
            username='test',
            email='test@test.com',
            password='test'
        )
        models.UserShow.objects.create(
            user=user,
            show_source=show_source
        )

    def tearDown(self):
        models.UserShow.objects.all().delete()
        models.auth_models.User.objects.all().delete()
        models.ShowSource.objects.all().delete()
        models.Show.objects.all().delete()
        models.Source.objects.all().delete()

    def assertCount(self, count_property, count):
        count_property = '{}_count'.format(count_property)
        total_count = sum(count)

        self.assertEqual(
            getattr(
                models.Show.objects.get(name='Show1'), count_property
            ),
            total_count,
            '{} should be {}'.format(count_property, total_count)
        )
        self.assertSequenceEqual(
            models.ShowEpisode.objects.filter(show__name='Show1').order_by(
                'episode'
            ).values_list(count_property, flat=True),
            count,
            'The episodes {} should be {}'.format(count_property, count)
        )
        self.assertEqual(
            getattr(
                models.UserShow.objects.get(user__username='test'),
                count_property
            ),
            total_count,
            '{} should be {}'.format(count_property, total_count)
        )

    def test_update_scores(self):
        user_show = models.UserShow.objects.get(user__username='test')

        comment1 = user_show.comments.create(episode=1, liked=True)
        self.assertCount('like', [1])
        self.assertCount('neutral', [0])
        self.assertCount('dislike', [0])

        comment2 = user_show.comments.create(episode=2)
        self.assertCount('like', [1, 0])
        self.assertCount('neutral', [0, 1])
        self.assertCount('dislike', [0, 0])

        comment3 = user_show.comments.create(episode=3, liked=False)
        self.assertCount('like', [1, 0, 0])
        self.assertCount('neutral', [0, 1, 0])
        self.assertCount('dislike', [0, 0, 1])

        comment1.liked = None
        comment1.save()
        self.assertCount('like', [0, 0, 0])
        self.assertCount('neutral', [1, 1, 0])
        self.assertCount('dislike', [0, 0, 1])

        comment3.liked = None
        comment3.save()
        self.assertCount('like', [0, 0, 0])
        self.assertCount('neutral', [1, 1, 1])
        self.assertCount('dislike', [0, 0, 0])

        comment2.liked = True
        comment2.save()
        self.assertCount('like', [0, 1, 0])
        self.assertCount('neutral', [1, 0, 1])
        self.assertCount('dislike', [0, 0, 0])

        comment1.liked = False
        comment1.save()
        self.assertCount('like', [0, 1, 0])
        self.assertCount('neutral', [0, 0, 1])
        self.assertCount('dislike', [1, 0, 0])

        comment2.liked = False
        comment2.save()
        self.assertCount('like', [0, 0, 0])
        self.assertCount('neutral', [0, 0, 1])
        self.assertCount('dislike', [1, 1, 0])

        comment1.liked = True
        comment1.save()
        self.assertCount('like', [1, 0, 0])
        self.assertCount('neutral', [0, 0, 1])
        self.assertCount('dislike', [0, 1, 0])

        comment1.delete()
        self.assertCount('like', [0, 0, 0])
        self.assertCount('neutral', [0, 0, 1])
        self.assertCount('dislike', [0, 1, 0])

        comment2.delete()
        self.assertCount('like', [0, 0, 0])
        self.assertCount('neutral', [0, 0, 1])
        self.assertCount('dislike', [0, 0, 0])

        comment3.delete()
        self.assertCount('like', [0, 0, 0])
        self.assertCount('neutral', [0, 0, 0])
        self.assertCount('dislike', [0, 0, 0])
