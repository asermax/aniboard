from django import test
from django.utils import timezone
from django.core.files import uploadedfile
import datetime
from .. import models


class ShowSourceTestCase(test.TestCase):
    def setUp(self):
        # create a couple of test shows an it's sources
        mock_image = uploadedfile.SimpleUploadedFile(
            'mock',
            b'content',
            'image/png'
        )

        # sources
        source1 = models.Source.objects.create(name='Source1')

        # shows
        show1 = models.Show.objects.create(
            name='Show1',
            slug='show1',
            cover_image=mock_image
        )

        # showsources
        models.ShowSource.objects.create(
            show=show1,
            source=source1,
            starts_on=timezone.now()
        )

    def tearDown(self):
        models.ShowSource.objects.all().delete()
        models.Show.objects.all().delete()
        models.Source.objects.all().delete()

    def test_is_schedulable(self):
        show_source = models.ShowSource.objects.get()
        self.assertFalse(show_source.is_scheduleable())

        show_source.schedules.create()
        self.assertFalse(show_source.is_scheduleable())

        show_source.schedules.create(order=1, saturday=datetime.time())
        self.assertTrue(show_source.is_scheduleable())

    def test_creates_correct_date_when_one_already_exists(self):
        show_source = models.ShowSource.objects.get()

        # ep count
        show = show_source.show
        show.number_episodes = 10
        show.save()
        show_source.last_aired_episode = 9
        show_source.save()

        # schedule
        today = timezone.now()
        today = today.replace(second=0, microsecond=0)
        schedule = show_source.schedules.create(order=0)
        schedule[today.weekday()] = today.time()
        schedule.save()

        # there's a datetime today, already used
        show_source.airing_datetimes.create(airing_datetime=today, used=True)

        # create next airing datetime
        show_source.create_airing_datetimes()
        self.assertEqual(
            show_source.airing_datetimes.count(),
            2,
            'The airing datetime was created'
        )

        # check if it's the correct date
        correct_date = today + timezone.timedelta(days=7)
        airing_datetime = show_source.airing_datetimes.get(
            used=False, skip=False
        )
        self.assertEqual(
            correct_date,
            airing_datetime.airing_datetime,
            'The correct airing datetime was created'
        )
