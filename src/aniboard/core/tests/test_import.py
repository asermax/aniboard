from django import test
from django.conf import settings
from django.core import urlresolvers
from django.utils import timezone
import os
import datetime
import unittest
import freezegun
import requests_mock

from .. import models


class SimpleImportTestCase(test.TestCase):
    fixtures = ['fixtures/test_data.json']

    def setUp(self):
        self.client.login(username='admin', password='admin')

    def test_form_is_shown(self):
        response = self.client.get(urlresolvers.reverse('admin:import_shows'))

        self.assertTemplateUsed(response, 'admin/core/import_shows.html')
        self.assertContains(
            response,
            '<title>Import shows | Django site admin</title>'
        )
        self.assertContains(
            response,
            '<form method="post" enctype="multipart/form-data">',
        )

    def test_link_to_import_show(self):
        response = self.client.get(
            urlresolvers.reverse('admin:core_show_changelist')
        )

        url = urlresolvers.reverse('admin:import_shows')
        link = '<a href="{}">Import</a>'.format(url)
        self.assertContains(response, link, html=True)


class BaseImportTestCase(object):
    fixtures = ['fixtures/test_data.json']
    file_extension = None

    def setUp(self):
        self.client.login(username='admin', password='admin')

    def _import_file(self, file_name):
        file_name = '{}.{}'.format(file_name, self.file_extension)
        import_file_path = os.path.join(
            settings.BASE_DIR, 'aniboard', 'core', 'tests', 'files', 'import',
            file_name
        )
        image_path = os.path.join(
            settings.BASE_DIR, 'aniboard', 'core', 'tests', 'files', 'import',
            file_name
        )

        with open(import_file_path, 'rb') as import_file, \
                requests_mock.mock() as mock, \
                open(image_path, 'rb') as image_file:
            mock.get(
                'http://images.com/valid.png', content=image_file.read()
            )
            mock.get(
                'http://images.com/invalid.png',
                reason='Not Found',
                status_code=404
            )

            response = self.client.post(
                urlresolvers.reverse('admin:import_shows'),
                {'import_file': import_file},
                follow=True
            )

        return response

    def _get_messages(self, response):
        return list(m.message for m in response.context['messages'])

    def test_assert_redirects_to_changelist(self):
        response = self._import_file('single')

        self.assertRedirects(
            response, urlresolvers.reverse('admin:core_show_changelist')
        )

    @freezegun.freeze_time('04-01-2014')
    def test_single_import(self):
        # try to import the file
        response = self._import_file('single')

        self.assertListEqual(
            self._get_messages(response),
            ['Added Yahari Ore no Seishun Love Comedy wa Machigatteiru. Zoku']
        )

        # initial show
        shows = models.Show.objects.filter(
            name='Yahari Ore no Seishun Love Comedy wa Machigatteiru. Zoku'
        )
        self.assertTrue(shows.exists(), 'The show was added')

        show = shows.first()
        self.assertEqual(
            show.name_ja,
            'やはり俺の青春ラブコメはまちがっている。続',
            'The correct japanese name was assigned'
        )
        self.assertEqual(
            show.number_episodes,
            13,
            'The correct number of episode was assigned'
        )
        self.assertTrue(show.cover_image, 'The cover image exists')

        # show source
        self.assertEqual(
            show.sources.all().count(),
            1,
            'A show source was created'
        )

        show_source = show.show_sources.first()

        self.assertEqual(
            show_source.source.name,
            'TBS',
            'The show source has the correct source'
        )
        start_date = timezone.datetime(
            2015, 4, 2, 16, 46, tzinfo=timezone.get_current_timezone()
        )
        self.assertEqual(
            show_source.starts_on,
            start_date,
            'The show source has the correct start date'
        )

        # schedule and airing datetime
        schedules = show_source.schedules.all()
        self.assertTrue(schedules.exists(), 'The schedule was created')

        schedule = schedules.first()
        self.assertListEqual(
            list(schedule),
            [
                None, None, None, datetime.time(16, 46), None, None,
                None
            ],
            'The schedule is correct'
        )

        # the created airing datetime should be next saturday
        starts_on = timezone.datetime(
            2015, 4, 2, 16, 46, tzinfo=timezone.get_current_timezone()
        )

        correct_datetime = starts_on

        self.assertTrue(
            show_source.airing_datetimes.for_datetime(
                correct_datetime
            ).exists(),
            'The initial airing datetime was created'
        )

        # api datas
        mal = show.api_data.filter(backend__backend_name='mal')
        self.assertEqual(mal.count(), 1, 'The MAL API data was created')
        self.assertEqual(
            mal.first().api_id, '23847', 'The MAL id is correct'
        )
        hb = show.api_data.filter(backend__backend_name='hummingbird')
        self.assertEqual(hb.count(), 1, 'The Humminbird API data was created')
        self.assertEqual(
            hb.first().api_id, '20000', 'The Hummingbird id is correct'
        )
        anilist = show.api_data.filter(backend__backend_name='anilist')
        self.assertEqual(
            anilist.count(), 1, 'The Anilist API data was created'
        )
        self.assertEqual(
            anilist.first().api_id, '20698', 'The Anilist id is correct'
        )

    def test_multi_import(self):
        # try to import the file
        response = self._import_file('multi')

        self.assertListEqual(
            self._get_messages(response),
            [
                'Added Battle Spirits: Burning Soul',
                'Added Yahari Ore no Seishun Love Comedy wa Machigatteiru. '
                'Zoku'
            ]
        )

        self.assertTrue(
            models.Show.objects.filter(
                name='Battle Spirits: Burning Soul'
            ).exists(),
            'The first show was added'
        )
        self.assertTrue(
            models.Show.objects.filter(
                name='Yahari Ore no Seishun Love Comedy wa Machigatteiru. Zoku'
            ).exists(),
            'The second show was added'
        )

    def test_missing_fields(self):
        # try to import the file
        response = self._import_file('with_missing_fields')

        self.assertListEqual(
            self._get_messages(response),
            ['Added Yahari Ore no Seishun Love Comedy wa Machigatteiru. Zoku']
        )

        shows = models.Show.objects.filter(
            name='Yahari Ore no Seishun Love Comedy wa Machigatteiru. Zoku'
        )
        self.assertTrue(shows.exists(), 'The show was added')

        show = shows.first()
        self.assertIsNone(show.name_ja, 'The japanese name is undefined')
        self.assertIsNone(
            show.number_episodes, 'The episode count is undefined'
        )
        self.assertFalse(
            show.cover_image, 'The cover image is undefined'
        )
        self.assertFalse(
            show.api_data.filter(backend__backend_name='mal').exists(),
            'The MAL api data wasn\'t created'
        )

    def test_missing_required_fields(self):
        # try to import the file
        response = self._import_file('with_missing_required_fields')

        self.assertListEqual(
            self._get_messages(response),
            ['Entry number 0: Doesn\'t have a title, which is required']
        )

    def test_update_already_existing_show(self):
        # try to import the file
        response = self._import_file('already_existing_show')

        self.assertListEqual(
            self._get_messages(response),
            ['Updated Test Show 1']
        )

        self.assertEqual(
            models.Show.objects.all().count(), 1, 'A new show wasn\'t created'
        )
        show = models.Show.objects.all().first()

        # updated japanese name
        self.assertEqual(
            show.name_ja, 'テスト番組一番', 'The japanese name was updated'
        )

        # updated episode count
        self.assertEqual(
            show.number_episodes, 13, 'The episode count was updated'
        )

        # updated image
        self.assertNotEqual(
            os.path.basename(show.cover_image.path),
            'cover_image16032015051149.png',
            'The cover image was updated'
        )

    def test_update_existing_show_api_data(self):
        # add an api data to an already existing show
        models.Show.objects.all().first().api_data.create(
            backend=models.Backend.objects.get(backend_name='hummingbird'),
            api_id='1111'
        )

        # try to import the file
        response = self._import_file(
            'already_existing_show_update_api_data'
        )

        self.assertListEqual(
            self._get_messages(response),
            ['Updated Test Show 1']
        )

        show = models.Show.objects.all().first()
        # check the hummingbird api data was updated
        self.assertEqual(
            show.api_data.get(backend__backend_name='hummingbird').api_id,
            '20000',
            'The api data was correctly updated'
        )

        # check the mal api data was created
        api_data = show.api_data.filter(backend__backend_name='mal').first()

        self.assertIsNotNone(api_data, 'The api data was created')
        self.assertEqual(api_data.api_id, '23847', 'The api data is correct')

    @freezegun.freeze_time('04-01-2014')
    def test_update_existing_show_source(self):
        # add a schedule and a initial date to an existing show source
        show_source = models.ShowSource.objects.all().first()
        show_source.schedules.create(monday=datetime.time(12))
        show_source.create_airing_datetimes()
        del show_source

        # try to import the file
        response = self._import_file(
            'already_existing_show_update_show_source'
        )

        self.assertListEqual(
            self._get_messages(response),
            ['Updated Test Show 1']
        )

        # check there's still only one schedule
        self.assertEqual(
            models.ShowSource.objects.all().count(),
            1,
            'Still a single show source'
        )

        show_source = models.ShowSource.objects.all().first()
        # check the correct source
        self.assertEqual(
            show_source.source.name,
            'Test Source 1',
            'The source hasn\'t changed'
        )

        # check the schedule and airing datetime got updated
        schedules = show_source.schedules.all()
        self.assertEqual(schedules.count(), 1, 'Still only one schedule')

        schedule = schedules.first()
        self.assertListEqual(
            list(schedule),
            [
                None, None, None, datetime.time(15), None, None,
                None
            ],
            'The schedule was updated correctly'
        )

        # the created airing datetime should be next saturday
        starts_on = timezone.datetime(
            2015, 4, 2, 15, tzinfo=timezone.get_current_timezone()
        )

        correct_datetime = starts_on

        self.assertEqual(
            show_source.airing_datetimes.all().count(),
            1,
            'Still only one airing datetime'
        )
        self.assertTrue(
            show_source.airing_datetimes.for_datetime(
                correct_datetime
            ).exists(),
            'The initial airing datetime was updated'
        )

    @freezegun.freeze_time('04-01-2014')
    def test_create_show_source_for_existing_show(self):
        # try to import the file
        response = self._import_file(
            'already_existing_show_update_show_source'
        )

        self.assertListEqual(
            self._get_messages(response),
            ['Updated Test Show 1']
        )

        show_source = models.ShowSource.objects.all().first()
        # check the correct source
        self.assertEqual(show_source.source.name, 'Test Source 1', 'TBS')

        # check the schedule and airing datetime got updated
        schedules = show_source.schedules.all()
        self.assertTrue(schedules.exists(), 'The schedule was created')

        schedule = schedules.first()
        self.assertListEqual(
            list(schedule),
            [
                None, None, None, datetime.time(15), None, None,
                None
            ],
            'The schedule is correct'
        )

        # the created airing datetime should be next saturday
        starts_on = timezone.datetime(
            2015, 4, 2, 15, tzinfo=timezone.get_current_timezone()
        )

        correct_datetime = starts_on

        self.assertTrue(
            show_source.airing_datetimes.for_datetime(
                correct_datetime
            ).exists(),
            'The initial airing datetime was created'
        )

    @unittest.skip
    def test_start_date_on_past(self):
        self.fail('Implement')

    def test_invalid_image_link(self):
        # try to import the file
        response = self._import_file('invalid_image_link')

        self.assertListEqual(
            self._get_messages(response),
            [
                'Entry number 0: The image couldn\'t be retrieved: '
                '404 Client Error: Not Found'
            ]
        )


class TextImportTestCase(BaseImportTestCase, test.TestCase):
    file_extension = 'txt'

    def test_incorrect_number_of_fields(self):
        # try to import the file
        response = self._import_file('incorrect_number_of_fields')

        self.assertListEqual(
            self._get_messages(response),
            ['Entry number 0: Has 9 fields instead of 10']
        )


class JSONImportTestCase(BaseImportTestCase, test.TestCase):
    file_extension = 'json'

    @freezegun.freeze_time('04-01-2014')
    def test_single_show_multiple_sources(self):
        # try to import the file
        response = self._import_file('single_show_multiple_sources')

        self.assertListEqual(
            self._get_messages(response),
            ['Added Yahari Ore no Seishun Love Comedy wa Machigatteiru. Zoku']
        )

        # initial show
        shows = models.Show.objects.filter(
            name='Yahari Ore no Seishun Love Comedy wa Machigatteiru. Zoku'
        )
        self.assertTrue(shows.exists(), 'The show was added')

        show = shows.first()
        # show sources
        self.assertEqual(
            show.sources.all().count(), 2, 'The two sources got created'
        )

    def test_create_with_syoboi_id(self):
        # try to import the file
        self._import_file('with_syoboi_id')

        show = models.Show.objects.get(
            name='Yahari Ore no Seishun Love Comedy wa Machigatteiru. Zoku'
        )

        self.assertEqual(show.syoboi_id, '111', 'The syoboi id was assigned')

    def test_update_already_existing_show_with_syoboi_id(self):
        # try to import the file
        self._import_file('already_existing_show_with_syoboi_id')

        show = models.Show.objects.get(name='Test Show 1')

        self.assertEqual(show.syoboi_id, '111', 'The syoboi id was assigned')
