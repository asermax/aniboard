from django.db import models as django_models
from django.utils import timezone
from django.contrib.auth import models as auth_models

import django_filters

from . import models, fields


def shows_not_watching(queryset, username):
    watching = models.UserShow.objects.filter(
        user__username=username, dropped=False)
    queryset = queryset.exclude(show_sources__user_shows__in=watching)

    return queryset


class ShowFilterSet(django_filters.FilterSet):
    not_watching = django_filters.CharFilter(action=shows_not_watching)

    class Meta:
        model = models.Show
        fields = ('slug', 'not_watching', 'airing')


class ShowEpisodeFilterSet(django_filters.FilterSet):
    show = django_filters.NumberFilter(name='show__id')
    slug = django_filters.CharFilter(name='show__slug')

    class Meta:
        model = models.ShowEpisode
        fields = ('show', 'slug', 'episode')


class ShowSourceFilterSet(django_filters.FilterSet):
    class Meta:
        model = models.ShowSource
        fields = ('show',)


def by_finished(queryset, finished):
    q_filter = django_models.Q(
        show_source__show__number_episodes__isnull=True
    ) | django_models.Q(last_episode_watched__lt=django_models.F(
        'show_source__show__number_episodes'))

    if finished:
        queryset = queryset.exclude(q_filter)
    else:
        queryset = queryset.filter(q_filter)

    return queryset


class UserShowFilterSet(django_filters.FilterSet):
    show = django_filters.NumberFilter(name='show_source__show__id')
    slug = django_filters.CharFilter(name='show_source__show__slug')
    dropped = django_filters.BooleanFilter(name='dropped')
    finished = django_filters.BooleanFilter(action=by_finished)
    user = django_filters.CharFilter('user__username')

    class Meta:
        model = models.UserShow
        fields = ('show', 'slug', 'dropped', 'finished', 'user')


def limit(queryset, value):
    return queryset[:value]


class UserShowActionFilterSet(django_filters.FilterSet):
    since = fields.PreciseDateTimeFilter(
        name='action_datetime', lookup_type='gt')
    user = django_filters.CharFilter('user_show__user__username')
    limit = django_filters.NumberFilter(action=limit)

    class Meta:
        model = models.UserShowAction
        fields = ('since', 'user', 'limit')


class UserShowCommentFilterSet(django_filters.FilterSet):
    show = django_filters.NumberFilter(name='user_show__show_source__show__id')
    slug = django_filters.CharFilter(name='user_show__show_source__show__slug')
    user = django_filters.CharFilter('user_show__user__username')
    stalked_by = django_filters.CharFilter(
        'user_show__user__stalked_by__stalker__username'
    )

    class Meta:
        model = models.UserShowComment
        fields = ('show', 'slug', 'episode', 'user', 'stalked_by')


class UserFilterSet(django_filters.FilterSet):
    stalked_by = django_filters.CharFilter('stalked_by__stalker__username')
    search = django_filters.CharFilter('username', lookup_type='icontains')

    class Meta:
        model = auth_models.User
        fields = ('search', 'stalked_by')


class UserTraceFilterSet(django_filters.FilterSet):
    date_time = fields.PreciseDateTimeFilter(
        name='date_time',
        lookup_type='gt',
    )

    class Meta:
        model = models.UserTrace
        fields = ('trace', 'date_time')


def latests_notices(queryset, latest):
    if latest:
        queryset = queryset.filter(
            active=True,
            date_time__gte=timezone.now() - timezone.timedelta(days=30),
        )

    return queryset


class NoticeFilterSet(django_filters.FilterSet):
    latest = django_filters.CharFilter(action=latests_notices)

    class Meta:
        model = models.Notice
        fields = ('latest',)
