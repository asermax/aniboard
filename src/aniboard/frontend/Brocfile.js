/* global require, module */

var EmberApp = require('ember-cli/lib/broccoli/ember-app');

var app = new EmberApp({
  vendorFiles: {
    'handlebars.js': null
  },
  fingerprint: {
    extensions: ['js', 'css', 'png', 'jpg', 'gif', 'ico', 'svg'],
    prepend: '/static/'
  }
});

app.import('bower_components/normalize.css/normalize.css');
app.import('vendor/styles/entypo.css');
app.import('vendor/styles/fontello.css');
app.import('vendor/scripts/ember-easyForm.js');
app.import('bower_components/moment/moment.js');
app.import('bower_components/moment-timezone/builds/moment-timezone-with-data-2010-2020.js');
app.import('bower_components/jstimezonedetect/jstz.js');
app.import('bower_components/marked/lib/marked.js');
app.import('bower_components/purl/purl.js');
app.import('bower_components/jquery.cookie/jquery.cookie.js');
app.import('bower_components/jquery.transit/jquery.transit.js');

// Use `app.import` to add additional libraries to the generated
// output files.
//
// If you need to use different assets in different
// environments, specify an object as the first parameter. That
// object's keys should be the environment name and the values
// should be the asset to use in that environment.
//
// If the library that you are including contains AMD or ES6
// modules that you would like to import into your application
// please specify an object with the list of modules as keys
// along with the exports of each module as its value.

module.exports = app.toTree();
