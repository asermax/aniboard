`import Ember from 'ember'`
`import DS from 'ember-data'`

ApplicationAdapter = DS.RESTAdapter.extend
  namespace: 'api'
  headers: (->
    'X-CSRFToken': $.cookie 'csrftoken'
  ).property().volatile()

  pathForType: (type) ->
    Ember.String.pluralize Ember.String.decamelize type

  buildURL: (type, id, snapshot) ->
    url = @_super type, id, snapshot

    if url.charAt(url.length - 1) isnt '/'
      url += '/'

    url

  createRecord: (store, type, snapshot) ->
    data = {}
    serializer = store.serializerFor type.typeKey
    serializer.serializeIntoHash data, type, snapshot, includeId: true
    data = data[type.typeKey]

    @ajax @buildURL(type.typeKey, null, snapshot), 'POST', data: data

  updateRecord: (store, type, snapshot) ->
    data = {}
    serializer = store.serializerFor type.typeKey
    serializer.serializeIntoHash data, type, snapshot
    data = data[type.typeKey]

    id = snapshot.id

    @ajax @buildURL(type.typeKey, id, snapshot), 'PUT', data: data

  ajaxError: (jqXHR) ->
    error = @_super(jqXHR)

    if jqXHR? and jqXHR.status is 400
      error = new DS.InvalidError errors: Ember.$.parseJSON jqXHR.responseText

    error

# inflector
Ember.Inflector.inflector.uncountable 'settings'

`export default ApplicationAdapter`
