`import Ember from 'ember'`
`import SettingsModel from '../models/settings'`

Settings = Ember.ObjectProxy.extend Ember.PromiseProxyMixin,
  content: null

  load: ->
    # retrieve user settings
    @set 'promise', new Ember.RSVP.Promise (resolve, reject) =>
      session = @get 'session'

      if Ember.isPresent Ember.keys session.store.restore()
        adapter = @store.adapterFor SettingsModel

        Ember.$.ajax
          type: 'GET'
          url: adapter.buildURL 'settings'
          success: (data) =>
            Ember.run =>
              resolve @store.push 'settings',
                @store.normalize('settings', data)
          error: (jqXHR, textStatus, errorThrown) =>
            Ember.run =>
              # something went wrong, most probably the authentication failed
              # so we need to wait for it to authenticate again
              session.one 'sessionAuthenticationSucceeded', @, ->
                @load()

              reject errorThrown
      else
        session.one 'sessionAuthenticationSucceeded', @, ->
          @load()

        reject new Error 'Not yet logged in'

  unload: ->
    @set 'content', null

  save: ->
    @get('content').save()

  rollback: ->
    @get('content').rollback()

# theme list
THEMES =
  default: '/assets/themes/default/theme.css'
  pink: '/assets/themes/pink/theme.css'
  inverted: '/assets/themes/inverted/theme.css'

`export {THEMES, Settings}`
`export default Settings`
