`import Ember from 'ember'`
`import DS from 'ember-data'`

BasePaginatedArray = DS.PromiseArray.extend
  params:
    perPage: 'page_size'
    totalPages: 'pages'
    currentPage: 'page'
  totalPages: null
  store: null
  modelName: null
  opts: {}
  currentPage: 1
  perPage: 15
  fetchOnInit: true

  init: ->
    @_super()
    if @get 'fetchOnInit'
      @fetchPage()

  then: (success, failure) ->
    @get('promise').then (=>
      success @
    ), failure

  _fetch: ->
    opts = @get 'opts'
    params = @get 'params'

    opts[params.perPage] = @get 'perPage'
    opts[params.currentPage] = @get 'currentPage'

    @store.find(@get('modelName'), opts)
    .then (result) =>
      @set 'totalPages', result.meta.pagination[params.totalPages]
      result.toArray()

PaginatedArray = BasePaginatedArray.extend
  clearOnFetch: false

  fetchPage: Ember.observer 'currentPage', ->
    currentPage = @get 'currentPage'
    totalPages = @get 'totalPages'

    if currentPage > 0 and (not totalPages? or currentPage <= totalPages)
      @fetch()

  fetch: ->
    if @get('clearOnFetch')
      @clear()

    @set 'promise', @_fetch()

  update: ->
    @_fetch().then (result) =>
      result.forEach ((item, index) ->
        while true
          current = @objectAt index

          if current == item
            break
          else if current and not result.contains current
            @removeObject current
          else
            @insertAt index, item
        return
      ), @

      if @get('length') > result.length
        @removeObjects @slice result.length

InfinitePaginatedArray = BasePaginatedArray.extend
  loadingMore: false

  reload: ->
    @set 'loadedPage', 0

  loadedPage: Ember.computed 'currentPage', (key, value, previousValue) ->
    if arguments.length > 1
      totalPages = @get 'totalPages'

      if value is 0
        # reload
        @set 'currentPage', 1
        @fetchPage true
      else if value > previousValue and (not totalPages? or value <= totalPages)
        # load the page
        @set 'currentPage', value
        @fetchPage()

    return @get 'currentPage'

  fetchPage: (reload=false) ->
    promise = @_fetch()

    if reload or not @get 'content'
      @set 'promise', promise
    else
      @set 'loadingMore', true

      promise.then((result) =>
        @pushObjects result
      ).finally =>
        @set 'loadingMore', false

`export {PaginatedArray, InfinitePaginatedArray}`
