`import Ember from 'ember'`

ScrollIntoViewMixin = Ember.Mixin.create
  didAnimateIn: ->
    top = @$().offset().top
    height = @$().height()
    scrollTop = $(window).scrollTop()
    scrollHeight = $(window).height()

    if top + height > scrollTop + scrollHeight
      $('html,body').animate scrollTop: top - (scrollHeight - height) / 2

`export default ScrollIntoViewMixin`
