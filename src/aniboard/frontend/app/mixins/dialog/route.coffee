`import Ember from 'ember'`

DialogRouteMixin = Ember.Mixin.create
  renderTemplate: ->
    @render
      outlet: 'dialog'
      into: 'application'

`export default DialogRouteMixin`
