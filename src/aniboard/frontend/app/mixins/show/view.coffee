`import Ember from 'ember'`
`import FadeViewMixin from '../fade-view'`

ShowViewMixin = Ember.Mixin.create FadeViewMixin,
  tagName: 'article'
  classNames: ['show']

`export default ShowViewMixin`
