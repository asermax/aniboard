`import Ember from 'ember'`
`import {PaginatedArray} from '../../structures/paginated-array'`
`import AuthenticatedRouteMixin from 'simple-auth/mixins/authenticated-route-mixin'`

ActionsIndexRoute = Ember.Route.extend AuthenticatedRouteMixin,
  model: (params) ->
    PaginatedArray.create
      store: @store
      modelName: 'user'
      perPage: 10
      currentPage: params.page or 1
      clearOnFetch: true
      opts:
        'stalked_by': @get 'session.username'

`export default ActionsIndexRoute`
