`import Ember from 'ember'`
`import DialogRouteMixin from '../../mixins/dialog/route'`
`import UnauthenticatedRouteMixin from 'simple-auth/mixins/unauthenticated-route-mixin'`

AccountRegisterRoute = Ember.Route.extend DialogRouteMixin, UnauthenticatedRouteMixin,
  titleToken: 'Register'

  model: (params) ->
    @store.createRecord 'userRegistration'

`export default AccountRegisterRoute`
