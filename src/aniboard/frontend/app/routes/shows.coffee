`import Ember from 'ember'`
`import AuthenticatedRouteMixin from 'simple-auth/mixins/authenticated-route-mixin'`

ShowsRoute = Ember.Route.extend AuthenticatedRouteMixin,
  titleToken: 'Shows'

  model: ->
    @store.find(
      'show', not_watching: @get('session.username'), airing: 'True'
    ).then (data) ->
      data.toArray()

`export default ShowsRoute`
