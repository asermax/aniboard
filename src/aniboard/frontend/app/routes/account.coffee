`import Ember from 'ember'`
`import generateTitle from '../utils/generate-title'`
`import UnauthenticatedRouteMixin from 'simple-auth/mixins/unauthenticated-route-mixin'`

AccountRoute = Ember.Route.extend UnauthenticatedRouteMixin,
  title: (tokens) ->
    tokens.reverse()
    tokens.push 'Account'
    generateTitle tokens

  actions:
    closeDialog: ->
      # we take care of it ourselves
      @transitionTo 'landing'

`export default AccountRoute`
