`import Ember from 'ember'`
`import DialogRouteMixin from '../../mixins/dialog/route'`
`import AuthenticatedRouteMixin from 'simple-auth/mixins/authenticated-route-mixin'`

BackendDisconnectRoute = Ember.Route.extend DialogRouteMixin, AuthenticatedRouteMixin,
  titleToken: 'Disconnect'

  afterModel: (model, transition) ->
    if not model.get 'isConnected'
      @transitionTo 'backend.connect', model

`export default BackendDisconnectRoute`
