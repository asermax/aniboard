`import Ember from 'ember'`

UserRoute = Ember.Route.extend
  controllerName: 'userShows'
  viewName: 'userShows'

  model: (params) ->
    @set 'titleToken', params['user']

    @store.find('userShow',
      user: params['user']
      dropped: 'False'
    ).then (data) ->
      data.toArray()

  setupController: (controller, model) ->
    @_super controller, model

    controller.set 'canEdit', false

  resetController: (controller) ->
    controller.set 'canEdit', true

`export default UserRoute`
