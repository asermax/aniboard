`import Ember from 'ember'`
`import DialogRouteMixin from '../../mixins/dialog/route'`
`import AuthenticatedRouteMixin from 'simple-auth/mixins/authenticated-route-mixin'`

UserShowDropRoute = Ember.Route.extend DialogRouteMixin, AuthenticatedRouteMixin,
  titleToken: 'Drop'

`export default UserShowDropRoute`
