`import Ember from 'ember'`
`import DialogRouteMixin from '../../mixins/dialog/route'`
`import AuthenticatedRouteMixin from 'simple-auth/mixins/authenticated-route-mixin'`

SettingsChangePasswordRoute = Ember.Route.extend DialogRouteMixin, AuthenticatedRouteMixin,
  titleToken: 'Change Password'

  model: (params) ->
    @store.createRecord 'passwordChange',

  actions:
    closeDialog: ->
      # we take care of it ourselves
      @transitionTo 'settings'

`export default SettingsChangePasswordRoute`
