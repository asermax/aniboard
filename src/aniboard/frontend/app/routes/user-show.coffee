`import Ember from 'ember'`
`import AuthenticatedRouteMixin from 'simple-auth/mixins/authenticated-route-mixin'`
`import generateTitle from '../utils/generate-title'`

UserShowRoute = Ember.Route.extend AuthenticatedRouteMixin,
  title: (tokens) ->
    model = @modelFor @routeName
    tokens.reverse()
    tokens.push model.get 'name'
    generateTitle tokens

  model: (params) ->
    # gotta add the user to the query
    params['user'] = @get 'session.username'

    new Ember.RSVP.Promise (resolve, reject) =>
      @store.find('userShow', params).then((result) ->
        if result and result.get('length') is 1
          resolve result.get 'firstObject'
        else
          resolve()
      ).catch (error) ->
        reject error

  afterModel: (model, transition) ->
    if not model?
      @transitionTo 'show.add', transition.params.userShow.slug
    else if model.get 'dropped'
      @transitionTo 'show.add', model.get 'show'

  serialize: (model, params) ->
    slug: model.get 'slug'

  actions:
    closeDialog: ->
      # we take care of it ourselves
      @transitionTo 'userShows'

`export default UserShowRoute`
