`import Ember from 'ember'`
`import AuthenticatedRouteMixin from 'simple-auth/mixins/authenticated-route-mixin'`
`import generateTitle from '../utils/generate-title'`

BackendRoute = Ember.Route.extend AuthenticatedRouteMixin,
  titleToken: ->
    model = @modelFor @routeName
    model.get 'name'

  model: (params) ->
    new Ember.RSVP.Promise (resolve, reject) =>
      @store.find('backend', params).then((result) ->
        if result and result.get('length') is 1
          resolve result.get 'firstObject'
        else
          reject 'Backend not found'
      ).catch (error) ->
        reject error

  serialize: (model, params) ->
    backend_name: model.get 'backendName'

  actions:
    closeDialog: ->
      # we take care of it ourselves
      @transitionTo 'settings'

`export default BackendRoute`
