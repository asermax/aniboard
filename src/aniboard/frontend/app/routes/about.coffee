`import Ember from 'ember'`
`import AuthenticatedRouteMixin from 'simple-auth/mixins/authenticated-route-mixin'`

AboutRoute = Ember.Route.extend AuthenticatedRouteMixin,
  titleToken: 'About'

`export default AboutRoute`
