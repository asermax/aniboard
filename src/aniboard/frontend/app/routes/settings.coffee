`import Ember from 'ember'`
`import AuthenticatedRouteMixin from 'simple-auth/mixins/authenticated-route-mixin'`

SettingsRoute = Ember.Route.extend AuthenticatedRouteMixin,
  titleToken: 'Settings'

  model: ->
    @store.find 'backend'

  setupController: (controller, model) ->
    controller.set 'model', @get 'settings'
    controller.set 'backends', model

`export default SettingsRoute`
