`import Ember from 'ember'`
`import config from './config/environment'`

Router = Ember.Router.extend
  location: config.locationType

Router.map ->
  @route 'about', path: '/about', ->
  @route 'landing', path: '/', ->
    @resource 'account', ->
      @route 'login'
      @route 'register', ->
        @route 'done'
      @route 'activate', path: '/activate/:activation_code'
      @route 'forgot'
      @route 'recover', path: '/recover/:recovery_code'
  @resource 'settings', ->
    @resource 'backend', path: '/backend/:backend_name', ->
      @route 'connect'
      @route 'disconnect'
    @route 'changePassword', path: '/change-password'
  @resource 'userShows', path: '/watching', ->
    @resource 'userShow', path: '/:slug', ->
      @route 'edit'
      @route 'drop'
      @route 'rate'
      @resource 'comments', path: '/episode/:episode', ->
        @route 'index', path: '/'
        @route 'edit', path: 'comment'
  @resource 'shows', ->
    @resource 'show', path: '/:slug', ->
      @route 'add'
  @resource 'actions', path: 'stalk', ->
    @route 'index', path: '/'
    @route 'user', path: '/:user'
  @resource 'notices'
  @route 'user', path: '/users/:user'

  # catchall
  @route 'catchAll', path: '/*path'

`export default Router`
