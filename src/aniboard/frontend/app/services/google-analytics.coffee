`import Ember from 'ember'`
`import ENV from 'aniboard/config/environment'`

GoogleAnalyticsService = Ember.Object.extend
  init: ->
    @_super()

    if ENV.googleAnalytics
      # generate default tracking object
      ga 'create', 'UA-51727300-1', 'auto', siteSpeedSampleRate: 100

  setUnknownProperty: (key, value) ->
    ga 'set', key, value

  send: ->
    [].splice.call arguments, 0, 0, 'send'
    ga.apply null, arguments

`export default GoogleAnalyticsService`
