`import Ember from 'ember'`

RadioButtonComponent = Ember.Component.extend
  tagName: 'input'
  type: 'radio'
  attributeBindings: ['type', 'htmlChecked:checked', 'value', 'name']

  htmlChecked: Ember.computed 'value', 'checked', ->
    @get('value') is @get('checked')

  change: ->
    @set 'checked', @get 'value'

  _updateElementValue: Ember.observer 'htmlChecked', ->
    Ember.run.next @, ->
      @$().prop 'checked', @get 'htmlChecked'

`export default RadioButtonComponent`
