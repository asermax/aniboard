`import Ember from 'ember'`

OpenButtonComponent = Ember.Component.extend
  classNameBindings: ['open']
  open: false
  closeElement: 'html'

  click: ->
    closeElement = @get 'closeElement'
    open = @toggleProperty 'open'
    @sendAction 'action', open

    elementId = @get 'elementId'

    if open
      $(closeElement).on "click.openButton.#{elementId}", (event) =>
        if event.target != @get 'element'
          @click()
        true
      $(closeElement).on "keydown.openButton.#{elementId}", (event) =>
        if event.keyCode is 27
          @click()
    else
      @$().trigger 'close'
      $(closeElement).off "click.openButton.#{elementId}"
      $(closeElement).off "keydown.openButton.#{elementId}"

`export default OpenButtonComponent`
