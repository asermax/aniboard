`import Ember from 'ember'`

SavingButtonComponent = Ember.Component.extend
  tagName: 'button'
  classNameBindings: ['model.isSaving:saving']

  click: ->
    if not @get 'model.isSaving'
      @sendAction 'action', @get 'model'

`export default SavingButtonComponent`
