`import Ember from 'ember'`
`import ScrollEventsMixin from '../mixins/scroll-events'`

LazyLoaderComponent = Ember.Component.extend ScrollEventsMixin,
  debounce: 100

  didInsertElement: ->
    @bindScrolling debounce: @debounce
    @scrolled()

  willDestroyElement: ->
    @unbindScrolling()

  scrolled: ->
    windowTop = $(window).scrollTop()
    windowBottom = windowTop + $(window).height()

    elTop = @$().offset().top
    elBottom = elTop + @$().height()

    if @bind and windowTop <= elBottom and windowBottom >= elTop
      @sendAction()
      @unbindScrolling()

`export default LazyLoaderComponent`
