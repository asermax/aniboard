`import DS from 'ember-data'`

ShowEpisode = DS.Model.extend
  show: DS.belongsTo 'show', async: true
  episode: DS.attr 'number'
  score: DS.attr 'number'
  hasComment: DS.attr 'boolean'

  # show aliases
  name: Ember.computed.readOnly 'show.name'
  slug: Ember.computed.readOnly 'show.slug'
  coverImage: Ember.computed.readOnly 'show.coverImage'

  hasScore: Ember.computed.notEmpty 'score'

`export default ShowEpisode`
