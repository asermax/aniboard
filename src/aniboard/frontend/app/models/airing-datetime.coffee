`import Ember from 'ember'`
`import DS from 'ember-data'`

AiringDatetime = DS.Model.extend
  airingDatetime: DS.attr 'date'

  airingMoment: Ember.computed 'airingDatetime', ->
    moment @get 'airingDatetime'

`export default AiringDatetime`
