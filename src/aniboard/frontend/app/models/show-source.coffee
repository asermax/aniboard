`import Ember from 'ember'`
`import DS from 'ember-data'`

ShowSource = DS.Model.extend
  show: DS.belongsTo 'show'
  source: DS.belongsTo 'source'
  airingDatetimes: DS.hasMany 'airing-datetime'
  lastAiredEpisode: DS.attr 'number'
  isPremiere: DS.attr 'boolean'

  sourceNamePremiere: Ember.computed 'source.name', 'isPremiere', ->
    name = @get 'source.name'

    if @get('isPremiere')
      name = "#{name} (Premiere)"

    return name

  finishedAiring: Ember.computed 'show.numberEpisodes', 'lastAiredEpisode', ->
    @get('show.numberEpisodes') == @get('lastAiredEpisode')

  futureAiringDatetimes: Ember.computed 'airingDatetimes.@each.airingMoment', ->
    yesterday = moment().subtract 1, 'day'

    @get('airingDatetimes').filter (datetime) ->
      datetime.get('airingMoment').isAfter(yesterday, 'day')

  airingDays: Ember.computed 'futureAiringDatetimes', ->
    # we use the dates from today (already filtered) to next week
    nextWeek = moment().add 7, 'days'

    airingDatetimes = @get('futureAiringDatetimes').mapBy 'airingMoment'
    airingDatetimes = airingDatetimes.filter (datetime) ->
      datetime.isBefore(nextWeek, 'day')

    if not airingDatetimes.length
      if @get('finishedAiring')
        airingDays = [8]
      else
        airingDays = [7]
    else
      airingDays = airingDatetimes.map((datetime) -> datetime.day()).uniq()

    return airingDays

  airingDatetime: (day) ->
    airingDatetime = undefined
    futureAiringDatetimes = @get 'futureAiringDatetimes'

    if futureAiringDatetimes.length
      if day? and day >= 0 and day <= 6
        airingDatetime = futureAiringDatetimes.find (item) =>
          item.get('airingMoment').day() == day
      else
        airingDatetime = futureAiringDatetimes.get 'firstObject'

    airingDatetime.get('airingMoment') if airingDatetime?

`export default ShowSource`
