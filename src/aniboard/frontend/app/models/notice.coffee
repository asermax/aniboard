`import Ember from 'ember'`
`import DS from 'ember-data'`

Notice = DS.Model.extend
  dateTime: DS.attr 'date'
  text: DS.attr 'string'
  isRead: DS.attr 'boolean'

  moment: Ember.computed 'dateTime', ->
    moment @get 'dateTime'

  readUrl: Ember.computed ->
    adapter = @store.adapterFor Notice
    baseUrl = adapter.buildURL 'notice', @get('id'), @
    "#{baseUrl}read/"

  read: () ->
    new Ember.RSVP.Promise (resolve, reject) =>
      $.ajax(
        type: 'POST'
        url: @get 'readUrl'
        contentType: 'application/json'
      ).done((data) =>
        @store.push 'notice', @store.normalize('notice', data)

        resolve()
      ).fail ->
        reject()

`export default Notice`
