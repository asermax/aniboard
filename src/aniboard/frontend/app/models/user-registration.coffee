`import DS from 'ember-data'`

UserRegistration = DS.Model.extend
  email: DS.attr 'string'
  username: DS.attr 'string'
  password: DS.attr 'string'

`export default UserRegistration`
