`import DS from 'ember-data'`
`import EmberValidations from 'ember-validations'`

Feedback = DS.Model.extend EmberValidations.Mixin,
  type: DS.attr 'string', defaultValue: 'Problem'
  shortDescription: DS.attr 'string'
  details: DS.attr 'string'

  types: [
    'Problem'
    'Suggestion'
    'Other'
  ]

  validations:
    shortDescription:
      presence: true
    details:
      presence: true

`export default Feedback`
