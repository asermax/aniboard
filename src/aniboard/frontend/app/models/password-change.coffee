`import DS from 'ember-data'`
`import EmberValidations from 'ember-validations'`

PasswordChange = DS.Model.extend EmberValidations.Mixin,
  oldPassword: DS.attr 'string'
  newPassword: DS.attr 'string'
  newPasswordRepeat: DS.attr 'string'

  validations:
    oldPassword:
      presence: true
    newPassword:
      presence: true
    newPasswordRepeat:
      presence: true
      comparator:
        against: 'newPassword'
        comparator: (newPasswordRepeat, newPassword)->
          newPassword == newPasswordRepeat
        message: 'the passwords must coincide'

`export default PasswordChange`
