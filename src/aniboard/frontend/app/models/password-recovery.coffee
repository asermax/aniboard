`import DS from 'ember-data'`
`import EmberValidations from 'ember-validations'`

PasswordRecovery = DS.Model.extend EmberValidations.Mixin,
  recoveryCode: DS.attr 'string'
  password: DS.attr 'string'

  validations:
    password:
      presence: true

`export default PasswordRecovery`
