`import Ember from 'ember'`
`import DialogViewMixin from 'aniboard/mixins/dialog/view'`

LoginView = Ember.View.extend DialogViewMixin,
  classNames: ['login']

`export default LoginView`
