`import Ember from 'ember'`

AnimateWaitView = Ember._MetamorphView.extend
  animateOut: (done) ->
    promises = []

    @mutateChildViews (parentView, view) ->
      promises.push new Ember.RSVP.Promise (resolve) ->
        view.destroy resolve

    Ember.RSVP.all(promises).then done

`export default AnimateWaitView`
