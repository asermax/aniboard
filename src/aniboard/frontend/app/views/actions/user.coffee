`import Ember from 'ember'`
`import FadeViewMixin from '../../mixins/fade-view'`
`import WeekViewMixin from '../../mixins/week/view'`

ActionsUserView = Ember.View.extend FadeViewMixin, WeekViewMixin

`export default ActionsUserView`
