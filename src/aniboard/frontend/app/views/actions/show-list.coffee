`import Ember from 'ember'`
`import ShowDayListViewMixin from '../../mixins/show/day-list-view'`

ActionsShowListView = Ember.View.extend ShowDayListViewMixin,
  classNames: ['actions']

`export default ActionsShowListView`
