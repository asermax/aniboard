`import Ember from 'ember'`
`import DialogViewMixin from '../../mixins/dialog/view'`

UserShowRateView = Ember.View.extend DialogViewMixin,
  classNames: ['rate']

`export default UserShowRateView`
