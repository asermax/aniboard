`import Ember from 'ember'`
`import DialogViewMixin from '../../mixins/dialog/view'`

CommentsEditView = Ember.View.extend DialogViewMixin,
  didAnimateIn: ->
    @$('.input-comment textarea').focus()

`export default CommentsEditView`
