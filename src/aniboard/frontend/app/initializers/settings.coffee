`import Settings from '../structures/settings'`
`import Configuration from 'simple-auth/configuration'`

# Takes two parameters: container and app
initialize = (container, app) ->
  # register and inject store
  container.register 'settings:main', Settings
  app.inject 'settings', 'store', 'store:main'
  app.inject 'settings', 'session', Configuration.session

  # inject to controllers, components and routes
  app.inject 'controller', 'settings', 'settings:main'
  app.inject 'component', 'settings', 'settings:main'
  app.inject 'route', 'settings', 'settings:main'

  # setup the settings loading
  app.deferReadiness()
  settings = container.lookup 'settings:main'
  settings.load().catch((error) ->
    if error.messag != 'Not yet logged in'
      error
  ).finally -> app.advanceReadiness()

SettingsInitializer =
  name: 'settings'
  after: ['store', 'simple-auth']
  initialize: initialize

`export {initialize}`
`export default SettingsInitializer`
