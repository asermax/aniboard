`import Ember from 'ember'`

initialize = (container, app) ->
  # inject to router and controllers
  app.inject 'route', 'ga', 'service:google-analytics'
  app.inject 'controller', 'ga', 'service:google-analytics'

  # setup pageviews and userid setting
  gaService = container.lookup 'service:google-analytics'
  router = container.lookup 'router:main'
  session = container.lookup 'simple-auth-session:main'

  # send a pageview event when the router transitions
  router.on 'didTransition', ->
    gaService.send 'pageview',
      page: router.get 'url'
      title: window.document.title

  # setup user id when the session gets authenticated
  Ember.addObserver session, 'id', ->
    id = session.get 'id'

    if id
      gaService.set 'userId', id

GoogleAnalyticsInitializer =
  name: 'google-analytics-service'
  after: ['simple-auth']
  initialize: initialize

`export default GoogleAnalyticsInitializer`
