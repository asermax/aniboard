`import Ember from 'ember'`

timezone = jstz.determine().name()

formatMoment = (value, options) ->
  format = options.hash.format or 'DD/MM/YYYY HH:mm'
  value.tz(timezone).format format

FormatMomentHelper = Ember.Handlebars.makeBoundHelper formatMoment

`export { formatMoment }`

`export default FormatMomentHelper`
