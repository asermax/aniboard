`import Ember from 'ember'`

HeaderController = Ember.Controller.extend
  season: Ember.computed ->
    now = moment()

    if now.isBefore moment('0104', 'DDMM')
      'Winter'
    else if now.isBefore moment('0107', 'DDMM')
      'Spring'
    else if now.isBefore moment('0110', 'DDMM')
      'Summer'
    else
      'Autumn'

  year: Ember.computed ->
    moment().year()

`export default HeaderController`
