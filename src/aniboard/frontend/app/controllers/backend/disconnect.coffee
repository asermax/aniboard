`import Ember from 'ember'`

BackendDisconnectController = Ember.Controller.extend
  actions:
    disconnect: ->
      if not @get 'isDisconnecting'
        model = @get 'model'
        name = @get 'model.name'

        # try to disconnect
        @set 'hermes.info', "Disconnecting account from **#{name}**..."

        model.disconnect().then( =>
          @set 'hermes.success', "Successfully disconnected account from
          **#{name}**."

          # close the dialog
          @send 'closeDialog'
        ).catch =>
          @set 'hermes.error', "Failed to disconnect account from
          **#{name}**."

`export default BackendDisconnectController`
