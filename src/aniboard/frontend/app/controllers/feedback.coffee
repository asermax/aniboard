`import Ember from 'ember'`

FeedbackController = Ember.Controller.extend
  actions:
    send: ->
      if not @get 'model.isSaving'
        # try to send
        @set 'hermes.info','Sending feedback...'

        @get('model').save().then( =>
          @set 'hermes.success', 'Your feedback has been sent succesfully!'

          # close the dialog
          @send 'closeDialog'
        ).catch =>
          @set 'hermes.error', 'Failed to send feedback.'

`export default FeedbackController`
