`import Ember from 'ember'`

SettingsChangePasswordController = Ember.Controller.extend
  actions:
    changePassword: ->
      if not @get 'model.isSaving'
        model = @get 'model'

        # try to save
        @set 'hermes.info','Changing password...'

        model.save().then( =>
          @set 'hermes.success', 'Succesfully changed!'

          # close the dialog
          @send 'closeDialog'
        ).catch =>
          @set 'hermes.error',
            'Failed to change, check if you entered the correct password.'

`export default SettingsChangePasswordController`
