`import Ember from 'ember'`
`import PaginatedControllerMixin from '../../mixins/paginated-controller'`

ActionsIndexController = Ember.ArrayController.extend PaginatedControllerMixin,
  queryParams: ['page']

  topStalker: Ember.computed ->
    stalkTrace = @store.createRecord 'userTrace', trace: 'actions.index'
    stalkTrace.get 'topUser'

  actions:
    stalk: (user) ->
      user.stalk().then =>
        # update our list
        @get('model').update()

`export default ActionsIndexController`
