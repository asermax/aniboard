`import Ember from 'ember'`

ShowAddController = Ember.Controller.extend
  show: null
  sources: null

  actions:
    add: ->
      if not @get 'model.isSaving'
        name = @get 'model.name'
        model = @get 'model'

        # have to undrop, just in case
        @set 'model.dropped', false

        # try to save
        @set 'hermes.info', "Adding **#{name}** to your list..."

        model.save().then( =>
          @set 'hermes.success', "Successfully added **#{name}** to your
          list."

          # triger event indicating we added an user show
          @eventHub.trigger 'userShow:added', model

          # close the dialog
          @send 'closeDialog'
        ).catch =>
          @set 'hermes.error', "Failed to add **#{name}** to your list."

`export default ShowAddController`
