`import Ember from 'ember'`

UserShowRateController = Ember.Controller.extend
  actions:
    rate: ->
      if not @get 'model.isSaving'
        name = @get 'model.name'
        model = @get 'model'

        # try to save
        @set 'hermes.info', "Rating **#{name}**..."

        model.save().then( =>
          @set 'hermes.success', "Successfully rated **#{name}**."

          # close the dialog
          @send 'closeDialog'
        ).catch =>
          @set 'hermes.error', "Failed to rate **#{name}**."

`export default UserShowRateController`
