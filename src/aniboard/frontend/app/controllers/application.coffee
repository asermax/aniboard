`import Ember from 'ember'`

ApplicationController = Ember.Controller.extend
  createTrace: Ember.observer 'currentPath', ->
    currentPath = @get 'currentPath'

    if @get('session.isAuthenticated') and currentPath != 'loading'
      trace = @store.createRecord 'userTrace',
        trace: currentPath
        user: @get 'session.id'

      trace.save()

`export default ApplicationController`
