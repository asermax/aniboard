`import BaseValidator from 'ember-validations/validators/base'`

ComparatorValidator = BaseValidator.extend
  init: ->
    @_super()
    @dependentValidationKeys.pushObject @options.against

  call: ->
    property = @get('model').get @property
    against = @get('model').get @options.against

    if not @options.comparator property, against
      @errors.pushObject @options.message.fmt(property, against)

`export default ComparatorValidator`
