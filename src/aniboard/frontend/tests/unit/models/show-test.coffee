`import { test, moduleForModel } from 'ember-qunit'`

moduleForModel 'show', 'Show', {
  # Specify the other units that are required for this test.
  needs: [
    'model:show-source'
    'model:source'
    'model:airing-datetime'
  ]
}

test 'it exists', ->
  model = @subject()
  # store = @store()
  ok !!model
