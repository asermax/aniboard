`import { test, moduleForModel } from 'ember-qunit'`

moduleForModel 'feedback', 'Feedback', {
  # Specify the other units that are required for this test.
  needs: [
    'service:validations'
    'ember-validations@validator:local/presence'
  ]
}

test 'it exists', ->
  model = @subject()
  # store = @store()
  ok !!model
