`import { test, moduleForComponent } from 'ember-qunit'`

moduleForComponent 'simpleauth-backend', 'SimpleauthBackendComponent', {
  # specify the other units that are required for this test
  needs: [
    'service:validations'
    'ember-validations@validator:local/presence'
  ]
}

test 'it renders', ->
  expect 0
  # expect 2

  # # creates the component instance
  # component = @subject()
  # equal component._state, 'preRender'

  # # appends the component to the page
  # @append()
  # equal component._state, 'inDOM'
