`import Ember from 'ember'`

testServerSetup = (app, fixture) ->
    if not fixture?
      fixture = 'fixtures/test_data.json'

    new Ember.RSVP.Promise (resolve, reject) ->
      Ember.$.post('testserver/setup/', {fixture: fixture}).always -> resolve()

`export default Ember.Test.registerAsyncHelper('testServerSetup', testServerSetup)`
